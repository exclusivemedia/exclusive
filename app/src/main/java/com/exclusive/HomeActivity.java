package com.exclusive;

import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.exclusive.fragments.HomeFragment;
import com.exclusive.fragments.SearchFragment;
import com.exclusive.utils.views.NonSuitableViewPager;
import com.exclusive.adapter.ViewPagerAdapter;
import com.exclusive.fragments.DownloadsFragment;
import com.exclusive.firebasedb.FirebaseDatabaseHelper;
import com.exclusive.fragments.MenuFragment;
import com.google.android.material.bottomnavigation.BottomNavigationView;

public class HomeActivity extends AppCompatActivity {

    MenuItem prevMenuItem;
    BottomNavigationView bottomNavigationView;
    //This is our viewPager
    private NonSuitableViewPager viewPager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        //Initializing viewPager
        viewPager = findViewById(R.id.viewpager);
        viewPager.setOffscreenPageLimit(4);
        //Initializing the bottomNavigationView
        bottomNavigationView = findViewById(R.id.bottom_navigation);

        bottomNavigationView.setOnNavigationItemSelectedListener(
                item -> {
                    switch (item.getItemId()) {
                        case R.id.navigation_home:
                            viewPager.setCurrentItem(0);
                            break;
                        case R.id.navigation_search:
                            viewPager.setCurrentItem(1);
                            break;
                        case R.id.navigation_Downloads:
                            viewPager.setCurrentItem(2);
                            break;
                        case R.id.navigation_Menu:
                            viewPager.setCurrentItem(3);
                            break;
                    }
                    return false;
                });

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (prevMenuItem != null) {
                    prevMenuItem.setChecked(false);
                } else {
                    bottomNavigationView.getMenu().getItem(0).setChecked(false);
                }
                Log.d("page", "onPageSelected: " + position);
                bottomNavigationView.getMenu().getItem(position).setChecked(true);
                prevMenuItem = bottomNavigationView.getMenu().getItem(position);

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        //Disable ViewPager Swipe
        //  viewPager.setOnTouchListener((v, event) -> true);


        setupViewPager(viewPager);
        FirebaseDatabaseHelper.getInstance().updateUserDetails();
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());

        adapter.addFragment(new HomeFragment());
        adapter.addFragment(new SearchFragment());
        adapter.addFragment( new DownloadsFragment());
        adapter.addFragment(new MenuFragment());
        viewPager.setAdapter(adapter);
    }
}