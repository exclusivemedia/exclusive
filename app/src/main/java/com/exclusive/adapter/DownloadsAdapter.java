package com.exclusive.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import com.exclusive.MediaPlayerActivity;
import com.exclusive.R;
import com.exclusive.downloads.DirectoryHelper;
import com.exclusive.models.DownloadedVideoModel;

import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.util.ArrayList;


public class DownloadsAdapter extends RecyclerView.Adapter<DownloadsAdapter.ViewHolder> {

    private final DetailsListener listener;
    private final ArrayList<DownloadedVideoModel> al_video;
    private final Context context;
    private final Activity activity;

    public DownloadsAdapter(Context context, ArrayList<DownloadedVideoModel> al_video, Activity activity, DetailsListener listener) {
        this.al_video = al_video;
        this.context = context;
        this.activity = activity;
        this.listener = listener;
    }

    @NotNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_videos, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        Bitmap bMap = ThumbnailUtils.createVideoThumbnail(al_video.get(position).getStr_thumb(),
                MediaStore.Video.Thumbnails.MICRO_KIND);

        holder.iv_image.setImageBitmap(bMap);
        holder.text.setText(al_video.get(position).getStr_name());

        holder.cardView.setOnClickListener(view -> {
            Intent i = new Intent(context, MediaPlayerActivity.class);
            Bundle mBundle = new Bundle();
            mBundle.putString("link", al_video.get(position).getStr_path());
            mBundle.putString("title", al_video.get(position).getStr_name());
            i.putExtras(mBundle);
            activity.startActivity(i);
        });
        holder.appCompatImageView.setOnClickListener(view ->
                //  listener.OfferDetailsListener(al_video.get(position))
        {
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
            alertDialogBuilder.setMessage("Are you sure,You wanted to delete Movie?");
            alertDialogBuilder.setPositiveButton("Delete",
                    (arg0, arg1) -> {
                        String dir = Environment.getExternalStorageDirectory() + "/" + DirectoryHelper.ROOT_DIRECTORY_NAME + "/";
                        File file = new File(dir, al_video.get(position).getStr_name());
                        System.out.println("FILE" + file.toString());
                        boolean deleted = file.delete();
                        if (deleted) {
                            Toast.makeText(context, "Deleted", Toast.LENGTH_SHORT).show();
                            context.sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.fromFile(file)));
                            int newPosition = holder.getAdapterPosition();

                            deleteItem(newPosition);
                        } else {
                            Toast.makeText(context, "Not deleted", Toast.LENGTH_SHORT).show();
                        }
                    });

            alertDialogBuilder.setNegativeButton("No", (dialog, which) -> {

            });

            AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();


        });
    }

    @Override
    public int getItemCount() {

        return al_video.size();
    }

    private void deleteItem(int position) {
        al_video.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, al_video.size());
        listener.OfferDetailsListener(position);
    }

    public interface DetailsListener {
        void OfferDetailsListener(int position);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        private final AppCompatImageView iv_image;
        private final AppCompatImageView appCompatImageView;
        private final AppCompatTextView text;
        private final LinearLayout cardView;

        public ViewHolder(View v) {

            super(v);

            iv_image = v.findViewById(R.id.imgImage);
            cardView = v.findViewById(R.id.cardView);
            text = v.findViewById(R.id.txtTitle);
            appCompatImageView = v.findViewById(R.id.delete);
        }
    }

}

