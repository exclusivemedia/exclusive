package com.exclusive.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.exclusive.R;
import com.exclusive.models.EpisodeModel;

import java.util.List;

public class EpisodesAdapter extends RecyclerView.Adapter<EpisodesAdapter.Holder> {
    private final List<EpisodeModel> episodeModelArrayList;
    private final Context context;
    OnClickListener onClickListener;
    private boolean isPremiumAndHaveNotPurchasedYet = true;

    public EpisodesAdapter(Context context, List<EpisodeModel> episodeModelArrayList, OnClickListener onClickListener) {
        this.context = context;
        this.onClickListener = onClickListener;
        this.episodeModelArrayList = episodeModelArrayList;

    }

    @Override
    public int getItemCount() {
        return (null != episodeModelArrayList ? episodeModelArrayList.size() : 0);
    }

    @Override
    public void onBindViewHolder(Holder holder,
                                 int position) {

        EpisodeModel episodeModel = episodeModelArrayList.get(position);
        Glide.with(context).load(episodeModel.getPreviewImage()).into(holder.imgImage);
        holder.txtTitle.setText(episodeModel.getTitle());
        holder.txtSubTitle.setText(episodeModel.getEpisode_subtitle());
        if (isPremiumAndHaveNotPurchasedYet) {
            holder.downloads.setOnClickListener(null);
            holder.cardView.setOnClickListener(null);
            holder.relPremium.setVisibility(View.VISIBLE);
        } else {
            holder.downloads.setOnClickListener(view -> onClickListener.onDownloadClicked(position));
            holder.cardView.setOnClickListener(view -> onClickListener.onClicked(position));
            holder.relPremium.setVisibility(View.GONE);
        }
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        LayoutInflater mInflater = LayoutInflater.from(viewGroup.getContext());

        ViewGroup mainGroup = (ViewGroup) mInflater.inflate(R.layout.list_item_episode, viewGroup, false);
        return new Holder(mainGroup);

    }

    @SuppressLint("NotifyDataSetChanged")
    public void setPremiumAndHaveNotPurchasedYet(boolean isPremiumAndHaveNotPurchasedYet) {
        this.isPremiumAndHaveNotPurchasedYet = isPremiumAndHaveNotPurchasedYet;
        notifyDataSetChanged();
    }

    public interface OnClickListener {
        void onClicked(int position);

        void onDownloadClicked(int position);
    }

    static class Holder extends RecyclerView.ViewHolder {
        TextView txtTitle;
        TextView txtSubTitle;
        ImageView imgImage;
        AppCompatImageView downloads;
        LinearLayout cardView;
        RelativeLayout relPremium;

        public Holder(@NonNull View itemView) {
            super(itemView);
            txtTitle = itemView.findViewById(R.id.txtTitle);
            txtSubTitle = itemView.findViewById(R.id.txtSubTitle);
            imgImage = itemView.findViewById(R.id.imgImage);
            downloads = itemView.findViewById(R.id.downloads);
            cardView = itemView.findViewById(R.id.cardView);
            relPremium = itemView.findViewById(R.id.relPremium);
        }
    }
}
