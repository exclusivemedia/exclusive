package com.exclusive.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.exclusive.R;

import java.util.ArrayList;

public class HomepageAdapter extends RecyclerView.Adapter<HomepageAdapter.HomepageViewHolder> {
    private final ArrayList<String> arrayList;
    private final ArrayList<String> imageList;
    private final Context context;

    public HomepageAdapter(Context context, ArrayList<String> arrayList, ArrayList<String> imageList) {
        this.context = context;
        this.arrayList = arrayList;
        this.imageList = imageList;

    }

    @Override
    public int getItemCount() {
        return (null != arrayList ? arrayList.size() : 0);
    }

    @Override
    public void onBindViewHolder(@NonNull HomepageViewHolder holder,
                                 int position) {
        //Setting text over textview
        ((HomepageViewHolder) holder).title.setText(arrayList.get(position));
        if (imageList.size() > position) {
            Glide.with(context).load(imageList.get(position)).into(((HomepageViewHolder) holder).imageView);
        }
    }

    @NonNull
    @Override
    public HomepageViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        LayoutInflater mInflater = LayoutInflater.from(viewGroup.getContext());

        ViewGroup mainGroup = (ViewGroup) mInflater.inflate(R.layout.item_row, viewGroup, false);

        return new HomepageViewHolder(mainGroup);

    }

    public static class HomepageViewHolder extends RecyclerView.ViewHolder {

        public TextView title;
        public ImageView imageView;

        public HomepageViewHolder(View view) {
            super(view);
            this.title = (TextView) view.findViewById(R.id.cardTitle);
            this.imageView = (ImageView) view.findViewById(R.id.imagePost);

        }


    }

}
