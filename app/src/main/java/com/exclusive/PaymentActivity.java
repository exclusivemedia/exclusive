package com.exclusive;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.webkit.WebView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.exclusive.firebasedb.FirebaseDatabaseHelper;
import com.exclusive.firebasedb.PurchaseType;
import com.exclusive.utils.HashGenerationUtils;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.payu.base.models.ErrorResponse;
import com.payu.base.models.PayUPaymentParams;
import com.payu.checkoutpro.PayUCheckoutPro;
import com.payu.checkoutpro.utils.PayUCheckoutProConstants;
import com.payu.ui.model.listeners.PayUCheckoutProListener;
import com.payu.ui.model.listeners.PayUHashGenerationListener;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;

public class PaymentActivity extends AppCompatActivity {
    String amount_str = "0";
    String product_name = "0";
    PurchaseType type = PurchaseType.movie;
    String uniqueId = "";


    public static void start(Activity context, String amount, String product_name, String type, String uniqueId) {
        Intent starter = new Intent(context, PaymentActivity.class);
        starter.putExtra("amount", amount);
        starter.putExtra("product_name", product_name);
        starter.putExtra("type", type);
        starter.putExtra("uniqueId", uniqueId);
        context.startActivityForResult(starter, 1234);
    }

    public static String hashCal(String str) {
        byte[] hashseq = str.getBytes();
        StringBuilder hexString = new StringBuilder();
        try {
            MessageDigest algorithm = MessageDigest.getInstance("SHA-512");
            algorithm.reset();
            algorithm.update(hashseq);
            byte[] messageDigest = algorithm.digest();
            for (byte aMessageDigest : messageDigest) {
                String hex = Integer.toHexString(0xFF & aMessageDigest);
                if (hex.length() == 1) {
                    hexString.append("0");
                }
                hexString.append(hex);
            }
        } catch (NoSuchAlgorithmException ignored) {
        }
        return hexString.toString();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);
        amount_str = getIntent().getStringExtra("amount");
        product_name = getIntent().getStringExtra("product_name");
        type = PurchaseType.valueOf(getIntent().getStringExtra("type"));
        uniqueId = getIntent().getStringExtra("uniqueId");
        launchPayUMoneyFlow();
    }

    private void launchPayUMoneyFlow() {
        FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();
        FirebaseUser firebaseUser = firebaseAuth.getCurrentUser();
        if (firebaseUser == null) {
            return;
        }
        double amount = 0;
        try {
            amount = Double.parseDouble(amount_str);

        } catch (Exception e) {
            e.printStackTrace();
        }

        String txnId = System.currentTimeMillis() + "";
        //String txnId = "TXNID720431525261327973";
        String phone = "9876543210";
        String productName = product_name;
        String firstName = firebaseUser.getDisplayName();
        String email = firebaseUser.getEmail() + "";

        PayUPaymentParams.Builder builder = new PayUPaymentParams.Builder();
        builder.setAmount(String.valueOf(amount))
                .setIsProduction(true)
                .setProductInfo(productName)
                .setKey(getString(R.string.merchant_key))
                .setPhone(phone)
                .setTransactionId(txnId)
                .setFirstName(firstName)
                .setEmail(email)
                .setSurl("https://www.payumoney.com/mobileapp/payumoney/success.php")
                .setFurl("https://www.payumoney.com/mobileapp/payumoney/failure.php");
        PayUPaymentParams payUPaymentParams = builder.build();


        PayUCheckoutPro.open(
                this,
                payUPaymentParams,
                new PayUCheckoutProListener() {

                    @Override
                    public void onPaymentSuccess(@NonNull Object response) {
                        //Cast response object to HashMap
                        HashMap<String, Object> result = (HashMap<String, Object>) response;
                        String payuResponse = (String) result.get(PayUCheckoutProConstants.CP_PAYU_RESPONSE);
                        String merchantResponse = (String) result.get(PayUCheckoutProConstants.CP_MERCHANT_RESPONSE);
                        addDetailsOfPurchaseInFirebaseDB(payuResponse);
                    }

                    @Override
                    public void onPaymentFailure(@NonNull Object response) {
                        //Cast response object to HashMap
                        HashMap<String, Object> result = (HashMap<String, Object>) response;
                        String payuResponse = (String) result.get(PayUCheckoutProConstants.CP_PAYU_RESPONSE);
                        String merchantResponse = (String) result.get(PayUCheckoutProConstants.CP_MERCHANT_RESPONSE);
                        paymentFail();
                    }

                    @Override
                    public void onPaymentCancel(boolean isTxnInitiated) {
                        paymentFail();
                    }

                    @Override
                    public void onError(@NonNull ErrorResponse errorResponse) {
                        String errorMessage = errorResponse.getErrorMessage();
                        paymentFail();
                    }

                    @Override
                    public void setWebViewProperties(@Nullable WebView webView, @Nullable Object o) {
                        //For setting webview properties, if any. Check Customized Integration section for more details on this
                    }

                    @Override
                    public void generateHash(@NonNull HashMap<String, String> valueMap, @NonNull PayUHashGenerationListener hashGenerationListener) {
                        String hashName = valueMap.get(PayUCheckoutProConstants.CP_HASH_NAME);
                        String hashData = valueMap.get(PayUCheckoutProConstants.CP_HASH_STRING);
                        if (!TextUtils.isEmpty(hashName) && !TextUtils.isEmpty(hashData)) {
                            //Calculate hash
                            if (hashData != null) {
                                String hash = HashGenerationUtils.INSTANCE.generateHashFromSDK(
                                        hashData,
                                        getString(R.string.merchant_salt),
                                        null
                                );
                                HashMap<String, String> dataMap = new HashMap<>();
                                dataMap.put(hashName, hash);
                                hashGenerationListener.onHashGenerated(dataMap);
                            }
                        }
                    }
                }
        );

    }


    private void addDetailsOfPurchaseInFirebaseDB(String details) {
        FirebaseDatabaseHelper.getInstance().updateDetailsForPurchase(type, uniqueId, details);
        setResult(RESULT_OK);
        finish();
    }

    private void paymentFail() {
        setResult(RESULT_CANCELED);
        finish();
    }
}