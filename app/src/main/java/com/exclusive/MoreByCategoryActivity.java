package com.exclusive;

import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.exclusive.apis.Api;
import com.exclusive.models.PostsResponse;
import com.exclusive.utils.RecyclerItemClickListener;
import com.exclusive.utils.Utils;
import com.exclusive.adapter.OtherPageAdapter;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MoreByCategoryActivity extends AppCompatActivity {
    private RecyclerView recyclerView;
    ArrayList<String> arrayList = new ArrayList<>();
    ArrayList<String> imageList = new ArrayList<>();
    String categoryId = "";
    String type = "";
    String title = "";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_more_by_category);
        title = getIntent().getStringExtra("title");
        categoryId = getIntent().getStringExtra("id");
        type = getIntent().getStringExtra("type");
        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mToolbar.setTitle(title);
        mToolbar.setNavigationIcon(R.drawable.ic_arrow);
        mToolbar.setNavigationOnClickListener(v -> finish());
        loading(true);
        setImage(categoryId);
    }

    public void loading(boolean loading) {
        if (loading) {
            findViewById(R.id.loading).setVisibility(View.VISIBLE);
            findViewById(R.id.recyclerView).setVisibility(View.GONE);
        } else {
            findViewById(R.id.loading).setVisibility(View.GONE);
            findViewById(R.id.recyclerView).setVisibility(View.VISIBLE);
        }
    }

    private void setImage(String category) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Api.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create()) //Here we are using the GsonConverterFactory to directly convert json data to object
                .build();

        Api api = retrofit.create(Api.class);
        Call<List<PostsResponse>> call = null;
        Map<String, String> data = Utils.getOptions();
        data.put("categories", category);
        call = api.postsByMultiCateegory100(data);
        call.enqueue(new Callback<List<PostsResponse>>() {
            @Override
            public void onResponse(retrofit2.Call<List<PostsResponse>> call, Response<List<PostsResponse>> response) {
                if (response.body() != null && response.body().size() != 0) {
                    for (int i = 0; i < response.body().size(); i++) {
                        arrayList.add("" + response.body().get(i).getId());
                        imageList.add("" + (response.body().get(i).getJetpackFeaturedMediaUrl()));
                    }
                }
                setRecyclerView();
            }

            @Override
            public void onFailure(retrofit2.Call<List<PostsResponse>> call, Throwable t) {
                Toast.makeText(MoreByCategoryActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                Toast.makeText(MoreByCategoryActivity.this, "Failed", Toast.LENGTH_LONG).show();
                Log.e("Error Query", t.getMessage());
                t.printStackTrace();
                loading(false);
            }
        });
    }



    public String stripHtml(String html) {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            return Html.fromHtml(html, Html.FROM_HTML_MODE_LEGACY).toString();
        } else {
            return Html.fromHtml(html).toString();
        }
    }

    //Setting recycler view
    private void setRecyclerView() {

        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setLayoutManager(new GridLayoutManager(MoreByCategoryActivity.this, 3));//Linear Items
        // recyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, true));


        OtherPageAdapter adapter = new OtherPageAdapter(MoreByCategoryActivity.this, arrayList, imageList);

        recyclerView.setAdapter(adapter);// set adapter on recyclerview
        loading(false);
        recyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(MoreByCategoryActivity.this, recyclerView, new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        Intent i;
                        if (type.equalsIgnoreCase("movies")) {
                            i = new Intent(MoreByCategoryActivity.this, MovieDetailActivity.class);
                        } else {
                            i = new Intent(MoreByCategoryActivity.this, WebSeriesDetailActivity.class);
                        }
                        Bundle mBundle = new Bundle();
                        mBundle.putString("link", arrayList.get(position));
                        i.putExtras(mBundle);
                        startActivity(i);
                    }

                    @Override
                    public void onLongItemClick(View view, int position) {
                        // do whatever
                    }
                })
        );


        if (arrayList.size() == 0) {
            Toast.makeText(MoreByCategoryActivity.this, "Sorry ! No Result Found.", Toast.LENGTH_LONG).show();
        }

    }
}
