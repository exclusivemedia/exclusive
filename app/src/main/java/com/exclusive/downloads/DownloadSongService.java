package com.exclusive.downloads;

import android.app.DownloadManager;
import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.exclusive.R;

import java.util.Objects;

public class DownloadSongService extends IntentService {
    private static final String DOWNLOAD_PATH = "Download_path";
    private static final String DESTINATION_PATH = "Destination_path";
    private static final String Title_PATH = "Title_path";

    public DownloadSongService() {
        super("DownloadSongService");
    }

    public static Intent getDownloadService(final @NonNull Context callingClassContext, final @NonNull String downloadPath,
                                            final @NonNull String titleName,
                                            final @NonNull String destinationPath) {
        return new Intent(callingClassContext, DownloadSongService.class)
                .putExtra(DOWNLOAD_PATH, downloadPath)
                .putExtra(DESTINATION_PATH, destinationPath)
                .putExtra(Title_PATH, titleName);
    }


    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        String downloadPath = null;
        String destinationPath = null;
        String titlePath = null;
        if (intent != null) {
            downloadPath = intent.getStringExtra(DOWNLOAD_PATH);
            destinationPath = intent.getStringExtra(DESTINATION_PATH);
            titlePath = intent.getStringExtra(Title_PATH);
        }

        startDownload(downloadPath, destinationPath,titlePath);
    }

    private void startDownload(String downloadPath, String destinationPath,String titlePath) {
        Uri uri = Uri.parse(downloadPath); // Path where you want to download file.
        DownloadManager.Request request = new DownloadManager.Request(uri);
        request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_MOBILE | DownloadManager.Request.NETWORK_WIFI);  // Tell on which network you want to download file.
        request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);  // This will show notification on top when downloading the file.
        request.setDescription(getString(R.string.app_name)); // Title for notification.
        request.setDescription("Downloading a file"); // Title for notification.
        request.setVisibleInDownloadsUi(true);
        request.setDestinationInExternalPublicDir(destinationPath, titlePath);  // Storage directory path /*uri.getLastPathSegment()*/
        ((DownloadManager) Objects.requireNonNull(getSystemService(Context.DOWNLOAD_SERVICE))).enqueue(request); // This will start downloading
    }
}