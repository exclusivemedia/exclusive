package com.exclusive.utils;

import java.util.HashMap;
import java.util.Map;

public class Utils {
    public static Map<String, String> getOptions() {
        Map<String, String> data = new HashMap<>();
        data.put("per_page", "24");
        return data;
    }

    public static Map<String, String> getTop5() {
        Map<String, String> data = new HashMap<>();
        data.put("per_page", "5");
        return data;
    }
}
