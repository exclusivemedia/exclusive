package com.exclusive.utils;

import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;

import com.exclusive.R;


public class AppUpdateDialog extends Dialog
{

    private Context context;

    private  String title;
    private String description;
    private boolean isCancelable;


    public AppUpdateDialog(Context context, String title, String description, boolean isCancelable) {
        super(context);
        // TODO Day selector
        this.context = context;
        this.title = title;
        this.description = description;
        this.isCancelable = isCancelable;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.view_app_update);

        AppCompatTextView tvTitle = findViewById(R.id.tvTitle);
        AppCompatTextView tvDescription = findViewById(R.id.tvMessage);
        AppCompatTextView tvUpdate = findViewById(R.id.tvUpdateNow);

        AppCompatImageView ivClose = findViewById(R.id.ivClose);

        if (isCancelable)
            ivClose.setVisibility(View.VISIBLE);
        else
            ivClose.setVisibility(View.GONE);

        ivClose.setOnClickListener(view -> dismiss());


        if(!TextUtils.isEmpty(title))
            tvTitle.setText(title);
        else
            tvTitle.setVisibility(View.GONE);


        if(!TextUtils.isEmpty(description))
            tvDescription.setText(String.format(description));
        else
            tvDescription.setVisibility(View.GONE);

        tvUpdate.setOnClickListener(view -> {

            // getPackageName() from Context or Activity object
            final String appPackageName = context.getPackageName();

            try {
                context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://exclusivemovies.in" )));
            } catch (ActivityNotFoundException activityNotFoundException) {
                context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://exclusivemovies.in" )));
            }
        });

    }

}
