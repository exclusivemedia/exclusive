package com.exclusive;

import android.app.Application;
import android.content.Intent;
import android.util.Log;

import com.exclusive.onesignal.NotificationHelper;
import com.exclusive.utils.ConnectivityReceiver;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.upstream.DefaultHttpDataSourceFactory;
import com.google.android.exoplayer2.upstream.FileDataSourceFactory;
import com.google.android.exoplayer2.upstream.HttpDataSource;
import com.google.android.exoplayer2.upstream.cache.Cache;
import com.google.android.exoplayer2.upstream.cache.CacheDataSource;
import com.google.android.exoplayer2.upstream.cache.CacheDataSourceFactory;
import com.google.android.exoplayer2.upstream.cache.NoOpCacheEvictor;
import com.google.android.exoplayer2.upstream.cache.SimpleCache;
import com.google.android.exoplayer2.util.Util;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.onesignal.OSNotificationOpenResult;
import com.onesignal.OneSignal;

import org.json.JSONObject;

import java.io.File;

public class Exclusive extends Application implements OneSignal.NotificationOpenedHandler {

    private static final String DOWNLOAD_CONTENT_DIRECTORY = "downloads";
    private static Exclusive mInstance;

    private String userAgent;

    private File downloadDirectory;
    private Cache downloadCache;


    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
        userAgent = Util.getUserAgent(this, "MyApp");

        // OneSignal Initialization
        OneSignal.startInit(this)
                .inFocusDisplaying(OneSignal.OSInFocusDisplayOption.Notification)
                .unsubscribeWhenNotificationsAreDisabled(true)
                .setNotificationOpenedHandler(this)
                .init();
    }

    /**
     * Returns a {@link DataSource.Factory}.
     */
    public DataSource.Factory buildDataSourceFactory() {
        DefaultDataSourceFactory upstreamFactory =
                new DefaultDataSourceFactory(this, buildHttpDataSourceFactory());
        return buildReadOnlyCacheDataSource(upstreamFactory, getDownloadCache());
    }

    /**
     * Returns a {@link HttpDataSource.Factory}.
     */
    private HttpDataSource.Factory buildHttpDataSourceFactory() {
        return new DefaultHttpDataSourceFactory(userAgent);
    }

    /**
     * Returns whether extension renderers should be used.
     */
    public boolean useExtensionRenderers() {
        return false;
    }


    private synchronized Cache getDownloadCache() {
        if (downloadCache == null) {
            File downloadContentDirectory = new File(getDownloadDirectory(), DOWNLOAD_CONTENT_DIRECTORY);
            downloadCache = new SimpleCache(downloadContentDirectory, new NoOpCacheEvictor());
        }
        return downloadCache;
    }

    private File getDownloadDirectory() {
        if (downloadDirectory == null) {
            downloadDirectory = getExternalFilesDir(null);
            if (downloadDirectory == null) {
                downloadDirectory = getFilesDir();
            }
        }
        return downloadDirectory;
    }

    private static CacheDataSourceFactory buildReadOnlyCacheDataSource(
            DefaultDataSourceFactory upstreamFactory, Cache cache) {
        return new CacheDataSourceFactory(
                cache,
                upstreamFactory,
                new FileDataSourceFactory(),
                /* cacheWriteDataSinkFactory= */ null,
                CacheDataSource.FLAG_IGNORE_CACHE_ON_ERROR,
                /* eventListener= */ null);
    }


    public static synchronized Exclusive getInstance() {
        return mInstance;
    }

    public void setConnectivityListener(ConnectivityReceiver.ConnectivityReceiverListener listener) {
        ConnectivityReceiver.connectivityReceiverListener = listener;
    }

    @Override
    public void notificationOpened(OSNotificationOpenResult result) {
        JSONObject data = result.notification.payload.additionalData;
        String contentType = "default";//default-webseries-movie
        String contentID = "default";

        boolean haveToShowCustomNotification = false;
        boolean haveToShowCustomNotificationWithBigImage = false;
        String notificationTitle = "";
        String notificationMessage = "";
        String notificationImage = "";

        Log.e("OSNotificationPayload", "JSONObject : " + result.notification.payload.toJSONObject().toString());


        if (data != null) {
            contentType = data.optString("contentType", "default");
            contentID = data.optString("contentID", "0");
            Log.e("OneSignalExample", "contentType: " + contentType);
            Log.e("OneSignalExample", "contentID: " + contentID);
        }

        if (data != null) {
            notificationTitle = data.optString("notificationTitle", "");
            notificationMessage = data.optString("notificationMessage", "");
            notificationImage = data.optString("notificationImage", "");
            haveToShowCustomNotification = data.optBoolean("haveToShowCustomNotification", false);
            haveToShowCustomNotificationWithBigImage = data.optBoolean("haveToShowCustomNotificationWithBigImage", false);
        }
        if (!checkIfUserAlreadyLogin()) {
            Intent intent = new Intent(getApplicationContext(), SplashActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            return;
        }
        if (contentType.equalsIgnoreCase("default")) {
            Intent intent = new Intent(getApplicationContext(), SplashActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        } else if (contentType.equalsIgnoreCase("webseries")) {
            Intent intent = new Intent(getApplicationContext(), WebSeriesDetailActivity.class);
            intent.putExtra("link", contentID);
            intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        } else if (contentType.equalsIgnoreCase("movie")) {
            Intent intent = new Intent(getApplicationContext(), MovieDetailActivity.class);
            intent.putExtra("link", contentID);
            intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }

        if (haveToShowCustomNotification) {
            Intent intent = new Intent(getApplicationContext(), SplashActivity.class);

            NotificationHelper notificationHelper = new NotificationHelper(getApplicationContext());
            notificationHelper.createNotification(notificationTitle,
                    notificationMessage,
                    R.drawable.ic_notification,
                    intent
            );
        }

        if (haveToShowCustomNotificationWithBigImage) {
            Intent intent = new Intent(getApplicationContext(), SplashActivity.class);

            NotificationHelper notificationHelper = new NotificationHelper(getApplicationContext());
            notificationHelper.createNotification(notificationTitle,
                    notificationMessage,
                    R.drawable.ic_notification,
                    notificationImage,
                    intent
            );
        }
    }

    private boolean checkIfUserAlreadyLogin() {
        try {
            FirebaseAuth mAuth = FirebaseAuth.getInstance();
            FirebaseUser currentUser = mAuth.getCurrentUser();
            if (currentUser != null) {
                Log.e("checkIfUserAlreadyLogin", "Yes user is logged in <3");
                return true;
            } else {
                Log.i("checkIfUserAlreadyLogin", "No user logged in :/");
                return false;
            }
        } catch (Exception e) {
            return false;
        }
    }
}