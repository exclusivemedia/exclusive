package com.exclusive;

import android.app.ProgressDialog;
import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.exclusive.utils.ConnectivityReceiver;

public class NoInternetActivity extends AppCompatActivity implements ConnectivityReceiver.ConnectivityReceiverListener {
    private Button btnCheck;
    ProgressDialog progress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_no_internet);


        btnCheck = (Button) findViewById(R.id.retry_button);

        btnCheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                checkConnection();


            }
        });
    }

    private void checkConnection() {

        boolean isConnected = ConnectivityReceiver.isConnected();
        showSnack(isConnected);

        progress = new ProgressDialog(this);

        progress.setMessage("Please Wait!!");
        progress.setCancelable(true);
        progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progress.show();

    }

    private void showSnack(boolean isConnected) {

        if (isConnected) {

           progress.dismiss();

        }
        else {

            Intent ne= new Intent(NoInternetActivity.this, NoInternetActivity.class);
            startActivity(ne);

            Toast.makeText(getApplicationContext(),"Sorry! Not connected to internet",Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        showSnack(isConnected);
    }

    @Override
    protected void onResume() {
        super.onResume();

        // register connection status listener
        Exclusive.getInstance().setConnectivityListener(this);
    }
}
