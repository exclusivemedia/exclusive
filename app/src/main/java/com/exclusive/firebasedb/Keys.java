package com.exclusive.firebasedb;

public class Keys {
    public static String FIREBASE_NODE_USERS = "users";
    public static String FIREBASE_NODE_PURCHASES = "purchases";
    public static String KEY_USERNAME = "username";
    public static String KEY_PROFILE_PIC = "profilePic";
    public static String KEY_EMAIL = "emailId";
}
