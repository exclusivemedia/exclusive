package com.exclusive.firebasedb;

import static com.exclusive.firebasedb.Keys.FIREBASE_NODE_PURCHASES;
import static com.exclusive.firebasedb.Keys.FIREBASE_NODE_USERS;
import static com.exclusive.firebasedb.Keys.KEY_EMAIL;
import static com.exclusive.firebasedb.Keys.KEY_PROFILE_PIC;
import static com.exclusive.firebasedb.Keys.KEY_USERNAME;

import android.util.Log;

import androidx.annotation.NonNull;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;

public class FirebaseDatabaseHelper {
    private static final String TAG = FirebaseDatabaseHelper.class.getSimpleName();
    private volatile static FirebaseDatabaseHelper instance;
    private final FirebaseDatabase firebaseDatabase;
    private final FirebaseAuth firebaseAuth;

    private FirebaseDatabaseHelper() {
        firebaseDatabase = FirebaseDatabase.getInstance();
        firebaseAuth = FirebaseAuth.getInstance();
    }

    public static FirebaseDatabaseHelper getInstance() {
        if (instance == null) {
            synchronized (FirebaseDatabaseHelper.class) {
                if (instance == null) {
                    instance = new FirebaseDatabaseHelper();
                }
            }
        }
        return instance;
    }

    public void updateUserDetails() {
        FirebaseUser firebaseUser = firebaseAuth.getCurrentUser();
        if (firebaseUser == null) {
            Log.d(TAG, "updateUserDetails: " + "no user logged in yet.");
            return;
        }
        String userId = firebaseUser.getUid();
        Log.d(TAG, "updateUserDetails: " + "userId : " + userId);
        HashMap<String, Object> dataMap = new HashMap<>();
        dataMap.put(KEY_USERNAME, firebaseUser.getDisplayName() + "");
        dataMap.put(KEY_PROFILE_PIC, firebaseUser.getPhotoUrl() + "");
        dataMap.put(KEY_EMAIL, firebaseUser.getEmail() + "");
        firebaseDatabase.getReference()
                .child(FIREBASE_NODE_USERS)
                .child(userId)
                .updateChildren(dataMap)
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        Log.d(TAG, "updateUserDetails: addOnCompleteListener : isSuccessful");
                    } else {
                        Log.d(TAG, "updateUserDetails: addOnCompleteListener : fail due to : " + task.getException());
                    }
                })
                .addOnFailureListener(task -> {
                    Log.d(TAG, "updateUserDetails: addOnFailureListener : " + task.getMessage());
                })
                .addOnCanceledListener(() -> {
                    Log.d(TAG, "updateUserDetails: addOnCanceledListener");
                });
    }

    public void checkIfAlreadyPurchased(PurchaseType purchaseType, String uniqueId, PurchasedHistoryListener purchasedHistoryListener) {
        FirebaseUser firebaseUser = firebaseAuth.getCurrentUser();
        if (firebaseUser == null) {
            Log.d(TAG, "checkIfAlreadyPurchased: " + "no user logged in yet.");
            return;
        }
        String userId = firebaseUser.getUid();
        Log.d(TAG, "checkIfAlreadyPurchased: " + "userId : " + userId);
        firebaseDatabase.getReference()
                .child(FIREBASE_NODE_PURCHASES)
                .child(userId)
                .child(purchaseType.toString())
                .child(uniqueId)
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot snapshot) {
                        purchasedHistoryListener.onPurchaseHistoryFetched(snapshot.exists());
                        Log.d(TAG, "checkIfAlreadyPurchased: onDataChange: " + snapshot.getValue());
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError error) {
                        purchasedHistoryListener.onPurchaseHistoryFetched(false);
                        Log.d(TAG, "checkIfAlreadyPurchased: onCancelled: " + error.getDetails());
                    }
                });
    }


    public void updateDetailsForPurchase(PurchaseType purchaseType, String uniqueId, String details) {
        FirebaseUser firebaseUser = firebaseAuth.getCurrentUser();
        if (firebaseUser == null) {
            Log.d(TAG, "updateDetailsForPurchase: " + "no user logged in yet.");
            return;
        }
        String userId = firebaseUser.getUid();
        Log.d(TAG, "updateDetailsForPurchase: " + "userId : " + userId);
        firebaseDatabase.getReference()
                .child(FIREBASE_NODE_PURCHASES)
                .child(userId)
                .child(purchaseType.toString())
                .child(uniqueId)
                .setValue(true/*details*/);

    }
}
