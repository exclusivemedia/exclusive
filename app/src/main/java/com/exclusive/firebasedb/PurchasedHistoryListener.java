package com.exclusive.firebasedb;

public interface PurchasedHistoryListener {
    void onPurchaseHistoryFetched(boolean isAlreadyPurchased);
}
