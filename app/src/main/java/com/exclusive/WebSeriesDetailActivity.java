package com.exclusive;

import android.Manifest;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.provider.Settings;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.exclusive.adapter.EpisodesAdapter;
import com.exclusive.adapter.OtherPageAdapter;
import com.exclusive.apis.Api;
import com.exclusive.downloads.DirectoryHelper;
import com.exclusive.downloads.DownloadSongService;
import com.exclusive.firebasedb.FirebaseDatabaseHelper;
import com.exclusive.firebasedb.PurchaseType;
import com.exclusive.models.EpisodeModel;
import com.exclusive.models.PostsResponse;
import com.exclusive.utils.RecyclerItemClickListener;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class WebSeriesDetailActivity extends AppCompatActivity {
    public static final String PROGRESS_UPDATE = "progress_update";
    TextView tvTitle;
    TextView postSubTitle;
    TextView tvDescription;
    ProgressBar progressBar;
    String videourl_hd;
    String videourl_sd;
    String videourl_low;
    String title;
    String backImage;
    String webseries_subtitle;
    String description;
    List<EpisodeModel> episodeModelArrayList = new ArrayList<>();
    RecyclerView recyclerView;
    RecyclerView recyclerViewMoreLikeThis;
    LinearLayout downloads, icon_group;
    LinearLayout linShare;
    TextView more_like;
    TextView episode;
    View viewEpisode;
    View viewMoreLikeThis;
    String value;
    ArrayList<String> arrayList = new ArrayList<>();
    ArrayList<String> imageList = new ArrayList<>();
    TextView txtPurchase;
    TextView txtWatchTrailer;
    String price;
    EpisodesAdapter episodesAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_series_detail);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Window w = getWindow();
            w.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        }
        value = getIntent().getExtras().getString("link");
        progressBar = findViewById(R.id.progressBar);
        icon_group = findViewById(R.id.icon_group);
        more_like = findViewById(R.id.more_like);
        episode = findViewById(R.id.episode);
        viewEpisode = findViewById(R.id.viewEpisode);
        viewMoreLikeThis = findViewById(R.id.viewMoreLikeThis);
        tvTitle = findViewById(R.id.postTitle);
        postSubTitle = findViewById(R.id.postSubTitle);
        tvDescription = findViewById(R.id.postDescription);
        recyclerView = findViewById(R.id.recyclerView);
        recyclerViewMoreLikeThis = findViewById(R.id.recyclerViewMoreLikeThis);
        txtPurchase = findViewById(R.id.txtPurchase);
        txtWatchTrailer = findViewById(R.id.txtWatchTrailer);
        findViewById(R.id.playBtn).setVisibility(View.GONE);
        findViewById(R.id.playBtn).setOnClickListener(v -> {
            Intent i = new Intent(getApplicationContext(), MediaPlayerActivity.class);
            Bundle mBundle = new Bundle();
            mBundle.putString("link", videourl_hd);
            mBundle.putString("link_sd", videourl_sd);
            mBundle.putString("link_low", videourl_low);
            mBundle.putString("title", title);
            i.putExtras(mBundle);
            startActivity(i);


        });
        progressBar.setVisibility(View.VISIBLE);
        icon_group.setVisibility(View.GONE);
        more_like.setVisibility(View.GONE);
        episode.setVisibility(View.GONE);
        viewEpisode.setVisibility(View.GONE);
        viewMoreLikeThis.setVisibility(View.GONE);
        txtPurchase.setVisibility(View.GONE);
        txtWatchTrailer.setVisibility(View.GONE);

        //getInfo();

        getInfoWebSeries(Integer.parseInt(value));

        findViewById(R.id.back).setOnClickListener(view -> finish());

        DirectoryHelper.createDirectory(this);
        downloads = findViewById(R.id.downloads);
        downloads.setOnClickListener(view -> {
            Toast.makeText(WebSeriesDetailActivity.this, "Coming soon...", Toast.LENGTH_SHORT).show();
            //requestStoragePermission(videourl, title);
        });

        linShare = findViewById(R.id.linShare);
        linShare.setOnClickListener(view -> {
            share();
        });


    }

    private void share() {
        progressBar.setVisibility(View.VISIBLE);
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());

        Picasso.get().load(backImage).into(new Target() {
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                progressBar.setVisibility(View.GONE);
                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.setType("image/*");
                intent.putExtra(Intent.EXTRA_STREAM, getBitmapFromView(bitmap));
                intent.putExtra(Intent.EXTRA_TEXT, getShareText());
                startActivity(Intent.createChooser(intent, "Share Image"));
            }

            @Override
            public void onBitmapFailed(Exception e, Drawable errorDrawable) {
                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {

            }
        });

    }

    private String getShareText() {
        return title + " is now streaming on Exclusive\n\nWatch your favourite Movies or Shows only on Exclusive" +
                "\n\nDownload the app Now\n\nhttps://bit.ly/3rABg65";
    }

    private Uri getBitmapFromView(Bitmap bmp) {
        Uri bmpUri = null;
        try {
            File file = new File(this.getExternalCacheDir(), System.currentTimeMillis() + ".jpg");

            FileOutputStream out = new FileOutputStream(file);
            bmp.compress(Bitmap.CompressFormat.JPEG, 90, out);
            out.close();
            bmpUri = Uri.fromFile(file);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return bmpUri;
    }


    /**
     * Requesting multiple permissions (storage and location) at once
     * This uses multiple permission model from dexter
     * On permanent denial opens settings dialog
     */
    private void requestStoragePermission(String videourl, String title) {
        Dexter.withContext(this)
                .withPermissions(
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        // check if all permissions are granted
                        if (report.areAllPermissionsGranted()) {
                            //      Toast.makeText(getApplicationContext(), "All permissions are granted!", Toast.LENGTH_SHORT).show();
                            startImageDownload(videourl, title);
                        }

                        // check for permanent denial of any permission
                        if (report.isAnyPermissionPermanentlyDenied()) {
                            // show alert dialog navigating to Settings
                            showSettingsDialog();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).
                withErrorListener(error -> Toast.makeText(getApplicationContext(), "Error occurred! ", Toast.LENGTH_SHORT).show())
                .onSameThread()
                .check();
    }

    private void showSettingsDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(WebSeriesDetailActivity.this);
        builder.setTitle("Need Permissions");
        builder.setMessage("This app needs permission to use this feature. You can grant them in app settings.");
        builder.setPositiveButton("GOTO SETTINGS", (dialog, which) -> {
            dialog.cancel();
            openSettings();
        });
        builder.setNegativeButton("Cancel", (dialog, which) -> dialog.cancel());
        builder.show();

    }

    // navigating user to app settings
    private void openSettings() {
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", getPackageName(), null);
        intent.setData(uri);
        startActivityForResult(intent, 101);
    }


    private void startImageDownload(String videourl, String title) {
        startService(DownloadSongService.getDownloadService(this, videourl, title,
                DirectoryHelper.ROOT_DIRECTORY_NAME.concat("/")));
    }

    private void getInfoWebSeries(int value) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Api.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create()) //Here we are using the GsonConverterFactory to directly convert json data to object
                .build();

        Api api = retrofit.create(Api.class);
        Call<PostsResponse> call;
        call = api.postsById(value);
        call.enqueue(new Callback<PostsResponse>() {
            @Override
            public void onResponse(retrofit2.Call<PostsResponse> call, Response<PostsResponse> response) {

                String dataOfWebSeries = response.body().getContent().getRendered();
                Log.e("data_of_web", "series => " + dataOfWebSeries);
                Log.e("data_of_web", "----------------------------------");
                dataOfWebSeries = stripHtml(dataOfWebSeries);

                JSONObject jsonObject = new JSONObject();
                try {
                    dataOfWebSeries = dataOfWebSeries.replaceAll("″", "\"");
                    dataOfWebSeries = dataOfWebSeries.replaceAll("”", "\"");
                    dataOfWebSeries = dataOfWebSeries.replaceAll("“", "\"");
                    jsonObject = new JSONObject(dataOfWebSeries);

                    backImage = jsonObject.optString("preview_image", "");
                    String featuredMediaUrl = jsonObject.optString("featuredMediaUrl", "");
                    videourl_hd = jsonObject.optString("trailer_video_url", "");
                    videourl_sd = jsonObject.optString("trailer_video_url_sd", "");
                    videourl_low = jsonObject.optString("trailer_video_url_low", "");
                    title = jsonObject.optString("webseries_title", "");
                    webseries_subtitle = jsonObject.optString("webseries_subtitle", "");
                    description = jsonObject.optString("description", "No Description");
                    price = jsonObject.optString("price", "0");
                    tvDescription.setText(stripHtml(description));
                    tvTitle.setText(title);
                    postSubTitle.setText(webseries_subtitle);
                    Glide.with(getApplicationContext())
                            .load(backImage)
                            .priority(Priority.IMMEDIATE)
                            .into((ImageView) findViewById(R.id.backgroundImage));

                    Glide.with(getApplicationContext())
                            .load(featuredMediaUrl)
                            .priority(Priority.IMMEDIATE)
                            .into((ImageView) findViewById(R.id.postImage));
                    progressBar.setVisibility(View.GONE);

                    icon_group.setVisibility(View.VISIBLE);
                    viewEpisode.setVisibility(View.VISIBLE);
                    episode.setVisibility(View.VISIBLE);


                    JSONArray jsonArray = jsonObject.getJSONArray("episodes");
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                        EpisodeModel episodeModel = new EpisodeModel();
                        episodeModel.setPreviewImage(jsonObject1.optString("preview_image", ""));
                        episodeModel.setTitle(jsonObject1.optString("episode_title", ""));
                        episodeModel.setVideourl_hd(jsonObject1.optString("episode_url", ""));
                        episodeModel.setVideourl_sd(jsonObject1.optString("episode_url_sd", ""));
                        episodeModel.setVideourl_low(jsonObject1.optString("episode_url_low", ""));
                        episodeModel.setEpisode_subtitle(jsonObject1.optString("episode_subtitle", ""));
                        episodeModelArrayList.add(episodeModel);
                    }
                    setRecyclerView();
                    setImage();

                    findViewById(R.id.playBtn).setVisibility(View.VISIBLE);
                    if (price.trim().isEmpty() || price.equals("0")) {
                        txtPurchase.setVisibility(View.VISIBLE);
                        txtPurchase.setText(R.string.watch_trailer);
                        txtPurchase.setOnClickListener(view -> findViewById(R.id.playBtn).performClick());
                        txtWatchTrailer.setVisibility(View.GONE);
                        if (episodesAdapter != null) {
                            episodesAdapter.setPremiumAndHaveNotPurchasedYet(false);
                        }
                    } else {
                        txtPurchase.setVisibility(View.VISIBLE);
                        txtPurchase.setText(getString(R.string.purchase_text, price));
                        txtPurchase.setOnClickListener(v -> PaymentActivity.start(WebSeriesDetailActivity.this, price, title, PurchaseType.web_series.name(), value + ""));
                        txtWatchTrailer.setVisibility(View.VISIBLE);
                        txtWatchTrailer.setOnClickListener(view -> findViewById(R.id.playBtn).performClick());
                        checkIfAlreadyPurchased();
                    }
                } catch (Exception error) {
                    Log.e("Exception", "series => " + error, error);
                }
                Log.e("jsonObject", "series => " + jsonObject.toString());

            }

            @Override
            public void onFailure(retrofit2.Call<PostsResponse> call, Throwable t) {
                Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
                Toast.makeText(getApplicationContext(), "Failed", Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1234 && resultCode == RESULT_OK) {
            checkIfAlreadyPurchased();
        }
    }

    private void checkIfAlreadyPurchased() {
        FirebaseDatabaseHelper.getInstance().checkIfAlreadyPurchased(PurchaseType.web_series, value, isAlreadyPurchased -> {
            if (isAlreadyPurchased) {
                txtPurchase.setVisibility(View.VISIBLE);
                txtPurchase.setText(R.string.watch_trailer);
                txtPurchase.setOnClickListener(view -> findViewById(R.id.playBtn).performClick());
                txtWatchTrailer.setVisibility(View.GONE);
                if (episodesAdapter != null) {
                    episodesAdapter.setPremiumAndHaveNotPurchasedYet(false);
                }
            }
        });
    }

    public String stripHtml(String html) {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            return Html.fromHtml(html, Html.FROM_HTML_MODE_LEGACY).toString();
        } else {
            return Html.fromHtml(html).toString();
        }
    }


    private void setRecyclerView() {

        recyclerView.setHasFixedSize(true);
        episodesAdapter = new EpisodesAdapter(getApplicationContext(), episodeModelArrayList, new EpisodesAdapter.OnClickListener() {
            @Override
            public void onClicked(int position) {
                Intent i = new Intent(getApplicationContext(), MediaPlayerActivity.class);
                Bundle mBundle = new Bundle();
                mBundle.putString("link", episodeModelArrayList.get(position).getVideourl_hd());
                mBundle.putString("link_sd", episodeModelArrayList.get(position).getVideourl_sd());
                mBundle.putString("link_low", episodeModelArrayList.get(position).getVideourl_low());
                mBundle.putString("title", episodeModelArrayList.get(position).getTitle());
                i.putExtras(mBundle);
                startActivity(i);
            }

            @Override
            public void onDownloadClicked(int position) {
                Toast.makeText(WebSeriesDetailActivity.this, "This Feature is Coming Soon", Toast.LENGTH_SHORT).show();
                /*requestStoragePermission(episodeModelArrayList.get(position).getUrl()
                        , episodeModelArrayList.get(position).getTitle());*/
            }
        });

        recyclerView.setAdapter(episodesAdapter);// set adapter on recyclerview


    }

    private void setImage() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Api.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create()) //Here we are using the GsonConverterFactory to directly convert json data to object
                .build();

        Api api = retrofit.create(Api.class);
        Call<List<PostsResponse>> call = null;
        call = api.postsByMultiCateegory("59");
        call.enqueue(new Callback<List<PostsResponse>>() {
            @Override
            public void onResponse(retrofit2.Call<List<PostsResponse>> call, Response<List<PostsResponse>> response) {

                if (response.body() != null && response.body().size() != 0) {
                    for (int i = 0; i < response.body().size(); i++) {
                        arrayList.add("" + response.body().get(i).getId());
                        imageList.add("" + (response.body().get(i).getJetpackFeaturedMediaUrl()));
                    }
                }
                setRecyclerViewMoreLikeThis();

            }

            @Override
            public void onFailure(retrofit2.Call<List<PostsResponse>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
                Toast.makeText(getApplicationContext(), "Failed", Toast.LENGTH_LONG).show();
            }
        });
    }

    private void setRecyclerViewMoreLikeThis() {

        recyclerViewMoreLikeThis.setHasFixedSize(true);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getApplicationContext(), 3, LinearLayoutManager.VERTICAL, false);
        recyclerViewMoreLikeThis.setLayoutManager(gridLayoutManager);

        OtherPageAdapter adapter = new OtherPageAdapter(getApplicationContext(), arrayList, imageList);

        recyclerViewMoreLikeThis.setAdapter(adapter);// set adapter on recyclerview
        recyclerViewMoreLikeThis.addOnItemTouchListener(
                new RecyclerItemClickListener(getBaseContext(), recyclerViewMoreLikeThis, new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {

                        Intent i = new Intent(WebSeriesDetailActivity.this, WebSeriesDetailActivity.class);
                        Bundle mBundle = new Bundle();
                        mBundle.putString("link", arrayList.get(position));
                        i.putExtras(mBundle);
                        startActivity(i);
                        finish();
                    }

                    @Override
                    public void onLongItemClick(View view, int position) {
                        // do whatever
                    }
                })
        );


        if (arrayList.size() > 1) {
            viewMoreLikeThis.setVisibility(View.VISIBLE);
            more_like.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}
