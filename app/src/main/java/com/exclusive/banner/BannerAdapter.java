package com.exclusive.banner;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.appcompat.widget.AppCompatImageView;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.bumptech.glide.Glide;
import com.exclusive.R;

import org.jetbrains.annotations.NotNull;

import java.util.List;

import static com.bumptech.glide.Priority.HIGH;


public class BannerAdapter extends PagerAdapter {

    private final DetailsListener listener;
    private Context context;
    private List<String> BannerList;

    public BannerAdapter(Context context, List<String> BannerList, DetailsListener listener) {
        this.context = context;
        this.BannerList = BannerList;
        this.listener = listener;
    }

    @Override
    public int getCount() {
        if (BannerList.size() > 5)
            return 5;
        else
            return BannerList.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @NotNull
    @Override
    public Object instantiateItem(@NotNull ViewGroup container, final int position) {

        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.banner_item, null);

        AppCompatImageView imageView = view.findViewById(R.id.iv_thumb);

        Glide.with(context).load(BannerList.get(position)).priority(HIGH).centerInside().into(imageView);

        imageView.setOnClickListener(v -> listener.OfferDetailsListener(position));

        ViewPager vp = (ViewPager) container;
        vp.addView(view, 0);
        return view;

    }

    @Override
    public void destroyItem(@NotNull ViewGroup container, int position, @NotNull Object object) {

        ViewPager vp = (ViewPager) container;
        View view = (View) object;
        vp.removeView(view);

    }

    public interface DetailsListener {
        void OfferDetailsListener(int position);
    }
}