package com.exclusive.apis;

import com.exclusive.models.PostsResponse;

import java.util.List;
import java.util.Map;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;
import retrofit2.http.Streaming;
import retrofit2.http.Url;

public interface Api{

    String BASE_URL = "https://exclusivemovies.in/";


    @GET("/wp-json/wp/v2/posts?filter[orderby]=rand&per_page=9")
    Call<List<PostsResponse>>postsByMultiCateegory(
            @Query("categories") String categories
    );
    @GET("/wp-json/wp/v2/posts?filter[orderby]=rand")
    Call<List<PostsResponse>>postsByMultiCateegory100(
            @QueryMap Map<String, String> options
    );

    @GET("/wp-json/wp/v2/posts")
    Call<List<PostsResponse>>postsBySearch(
            @Query("search") String search
    );
    @GET("/wp-json/wp/v2/posts")
    Call<List<PostsResponse>>postsBySearch2(
            @Query("search") String search,
            @QueryMap Map<String, String> options
    );
    @GET("/wp-json/wp/v2/posts/{id}")
    Call<PostsResponse>postsById(
            @Path("id") int id

    );

    @GET()
    @Streaming
    Call<ResponseBody> downloadImage(@Url String fileUrl);


}
