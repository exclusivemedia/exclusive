package com.exclusive;

import android.Manifest;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.provider.Settings;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.exclusive.adapter.OtherPageAdapter;
import com.exclusive.apis.Api;
import com.exclusive.downloads.DirectoryHelper;
import com.exclusive.downloads.DownloadSongService;
import com.exclusive.firebasedb.FirebaseDatabaseHelper;
import com.exclusive.firebasedb.PurchaseType;
import com.exclusive.models.PostsResponse;
import com.exclusive.utils.RecyclerItemClickListener;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MovieDetailActivity extends AppCompatActivity {
    public static final String PROGRESS_UPDATE = "progress_update";
    TextView tvTitle;
    TextView postSubTitle;
    TextView postSubTitle2;
    TextView postSubTitle3;
    TextView tvDescription;
    ProgressBar progressBar;
    String guid;
    String posterString;
    String videourl_hd;
    String videourl_sd;
    String videourl_low;
    String backImage;
    String title;
    String description;
    String movie_subtitle1;
    String movie_subtitle2;
    String movie_subtitle3;
    String movie_category_id;
    String price;
    ArrayList<String> arrayList = new ArrayList<>();
    ArrayList<String> imageList = new ArrayList<>();
    RecyclerView recyclerView;
    LinearLayout downloads, icon_group;
    TextView more_like;
    TextView txtPurchase;
    String value;

    LinearLayout linShare;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie_detail);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Window w = getWindow();
            w.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        }
        value = getIntent().getExtras().getString("link");
        progressBar = findViewById(R.id.progressBar);
        icon_group = findViewById(R.id.icon_group);
        more_like = findViewById(R.id.more_like);
        txtPurchase = findViewById(R.id.txtPurchase);
        tvTitle = findViewById(R.id.postTitle);
        postSubTitle = findViewById(R.id.postSubTitle);
        postSubTitle2 = findViewById(R.id.postSubTitle2);
        postSubTitle3 = findViewById(R.id.postSubTitle3);
        tvDescription = findViewById(R.id.postDescription);
        recyclerView = findViewById(R.id.recyclerView);
        findViewById(R.id.playBtn).setVisibility(View.GONE);
        findViewById(R.id.playBtn).setOnClickListener(v -> {
            Intent i = new Intent(getApplicationContext(), MediaPlayerActivity.class);
            Bundle mBundle = new Bundle();
            //mBundle.putString("link",guid);
            mBundle.putString("link", videourl_hd);
            mBundle.putString("link_sd", videourl_sd);
            mBundle.putString("link_low", videourl_low);
            mBundle.putString("title", title);
            i.putExtras(mBundle);
            startActivity(i);


        });
        progressBar.setVisibility(View.VISIBLE);
        icon_group.setVisibility(View.GONE);
        more_like.setVisibility(View.GONE);
        txtPurchase.setVisibility(View.GONE);


        getInfo(Integer.parseInt(value));

        findViewById(R.id.back).setOnClickListener(view -> finish());

        DirectoryHelper.createDirectory(this);
        downloads = findViewById(R.id.downloads);
        downloads.setOnClickListener(view -> {
            Toast.makeText(MovieDetailActivity.this, "This Feature is Coming soon", Toast.LENGTH_SHORT).show();
            //requestStoragePermission();
        });

        linShare = findViewById(R.id.linShare);
        linShare.setOnClickListener(view -> {
            share();
        });
    }

    private void share() {
        progressBar.setVisibility(View.VISIBLE);
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());

        Picasso.get().load(backImage).into(new Target() {
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                progressBar.setVisibility(View.GONE);
                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.setType("image/*");
                intent.putExtra(Intent.EXTRA_STREAM, getBitmapFromView(bitmap));
                intent.putExtra(Intent.EXTRA_TEXT, getShareText());
                startActivity(Intent.createChooser(intent, "Share Image"));
            }

            @Override
            public void onBitmapFailed(Exception e, Drawable errorDrawable) {
                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {

            }
        });

    }

    private String getShareText() {
        return title + " is now Streaming on Exclusive\n\nWatch your favourite Movies or Shows only on Exclusive" +
                "\n\nDownload the app Now\n\nhttps://bit.ly/3rABg65";
    }

    private Uri getBitmapFromView(Bitmap bmp) {
        Uri bmpUri = null;
        try {
            File file = new File(this.getExternalCacheDir(), System.currentTimeMillis() + ".jpg");

            FileOutputStream out = new FileOutputStream(file);
            bmp.compress(Bitmap.CompressFormat.JPEG, 90, out);
            out.close();
            bmpUri = Uri.fromFile(file);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return bmpUri;
    }


    /**
     * Requesting multiple permissions (storage and location) at once
     * This uses multiple permission model from dexter
     * On permanent denial opens settings dialog
     */
    private void requestStoragePermission() {
        Dexter.withContext(this)
                .withPermissions(
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        // check if all permissions are granted
                        if (report.areAllPermissionsGranted()) {
                            //      Toast.makeText(getApplicationContext(), "All permissions are granted!", Toast.LENGTH_SHORT).show();
                            startImageDownload();
                        }

                        // check for permanent denial of any permission
                        if (report.isAnyPermissionPermanentlyDenied()) {
                            // show alert dialog navigating to Settings
                            showSettingsDialog();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).
                withErrorListener(error -> Toast.makeText(getApplicationContext(), "Error occurred! ", Toast.LENGTH_SHORT).show())
                .onSameThread()
                .check();
    }

    private void showSettingsDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(MovieDetailActivity.this);
        builder.setTitle("Need Permissions");
        builder.setMessage("This app needs permission to use this feature. You can grant them in app settings.");
        builder.setPositiveButton("GOTO SETTINGS", (dialog, which) -> {
            dialog.cancel();
            openSettings();
        });
        builder.setNegativeButton("Cancel", (dialog, which) -> dialog.cancel());
        builder.show();

    }

    // navigating user to app settings
    private void openSettings() {
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", getPackageName(), null);
        intent.setData(uri);
        startActivityForResult(intent, 101);
    }


    private void startImageDownload() {
        startService(DownloadSongService.getDownloadService(this, videourl_hd, title,
                DirectoryHelper.ROOT_DIRECTORY_NAME.concat("/")));
    }


    private void getInfo(int value) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Api.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create()) //Here we are using the GsonConverterFactory to directly convert json data to object
                .build();

        Api api = retrofit.create(Api.class);
        Call<PostsResponse> call;
        call = api.postsById(value);
        call.enqueue(new Callback<PostsResponse>() {
            @Override
            public void onResponse(Call<PostsResponse> call, Response<PostsResponse> response) {

                String dataOfMovie = response.body().getContent().getRendered();
                Log.e("data_of_web", "series => " + dataOfMovie);
                Log.e("data_of_web", "----------------------------------");
                dataOfMovie = stripHtml(dataOfMovie);

                JSONObject jsonObject = new JSONObject();
                try {
                    dataOfMovie = dataOfMovie.replaceAll("″", "\"");
                    dataOfMovie = dataOfMovie.replaceAll("”", "\"");
                    dataOfMovie = dataOfMovie.replaceAll("“", "\"");
                    jsonObject = new JSONObject(dataOfMovie);

                    backImage = jsonObject.optString("movie_back_image", "");
                    String featuredMediaUrl = response.body().getJetpackFeaturedMediaUrl();
                    videourl_hd = jsonObject.optString("movie_video_url", "");
                    videourl_sd = jsonObject.optString("movie_video_url_sd", "");
                    videourl_low = jsonObject.optString("movie_video_url_low", "");
                    title = jsonObject.optString("movie_title", "");
                    description = jsonObject.optString("movie_description", "No Description");
                    movie_subtitle1 = jsonObject.optString("movie_subtitle1", "");
                    movie_subtitle2 = jsonObject.optString("movie_subtitle2", "");
                    movie_subtitle3 = jsonObject.optString("movie_subtitle3", "");
                    movie_category_id = jsonObject.optString("movie_category_id", "");
                    price = jsonObject.optString("price", "0");

                    tvDescription.setText(stripHtml(description));
                    tvTitle.setText(title);
                    postSubTitle.setText(movie_subtitle1);
                    postSubTitle2.setText(movie_subtitle2);
                    postSubTitle3.setText(movie_subtitle3);

                    txtPurchase.setVisibility(View.VISIBLE);
                    if (price.trim().isEmpty() || price.equals("0")) {
                        txtPurchase.setText(getString(R.string.watch_now_updated));
                        findViewById(R.id.playBtn).setVisibility(View.VISIBLE);
                        txtPurchase.setOnClickListener(view -> findViewById(R.id.playBtn).performClick());
                    } else {
                        findViewById(R.id.playBtn).setVisibility(View.GONE);
                        txtPurchase.setText(getString(R.string.purchase_text, price));
                        txtPurchase.setOnClickListener(v -> PaymentActivity.start(MovieDetailActivity.this, price, title, PurchaseType.movie.name(), value + ""));
                        checkIfAlreadyPurchased();
                    }

                    Glide.with(getApplicationContext())
                            .load(backImage)
                            .priority(Priority.IMMEDIATE)
                            .into((ImageView) findViewById(R.id.backgroundImage));

                    Glide.with(getApplicationContext())
                            .load(featuredMediaUrl)
                            .priority(Priority.IMMEDIATE)
                            .into((ImageView) findViewById(R.id.postImage));
                    progressBar.setVisibility(View.GONE);

                    progressBar.setVisibility(View.GONE);
                    icon_group.setVisibility(View.VISIBLE);
                    more_like.setVisibility(View.VISIBLE);

                    setImage(movie_category_id);
                } catch (Exception error) {
                    Log.e("Exception", "series => " + error, error);
                }
            }

            @Override
            public void onFailure(Call<PostsResponse> call, Throwable t) {
                Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
                Toast.makeText(getApplicationContext(), "Failed", Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1234 && resultCode == RESULT_OK) {
            checkIfAlreadyPurchased();
        }
    }

    private void checkIfAlreadyPurchased() {
        FirebaseDatabaseHelper.getInstance().checkIfAlreadyPurchased(PurchaseType.movie, value, isAlreadyPurchased -> {
            if (isAlreadyPurchased) {
                txtPurchase.setText(getString(R.string.watch_now_updated));
                findViewById(R.id.playBtn).setVisibility(View.VISIBLE);
                txtPurchase.setOnClickListener(view -> findViewById(R.id.playBtn).performClick());
            }
        });
    }

    public String stripHtml(String html) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            return Html.fromHtml(html, Html.FROM_HTML_MODE_LEGACY).toString();
        } else {
            return Html.fromHtml(html).toString();
        }
    }

    private void setImage(String category) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Api.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create()) //Here we are using the GsonConverterFactory to directly convert json data to object
                .build();

        Api api = retrofit.create(Api.class);
        Call<List<PostsResponse>> call = null;
        call = api.postsByMultiCateegory(category);
        call.enqueue(new Callback<List<PostsResponse>>() {
            @Override
            public void onResponse(Call<List<PostsResponse>> call, Response<List<PostsResponse>> response) {

                if (response.body() != null && response.body().size() != 0) {
                    for (int i = 0; i < response.body().size(); i++) {
                        arrayList.add("" + response.body().get(i).getId());
                        imageList.add("" + (response.body().get(i).getJetpackFeaturedMediaUrl()));
                    }
                }
                setRecyclerView();

            }

            @Override
            public void onFailure(Call<List<PostsResponse>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
                Toast.makeText(getApplicationContext(), "Failed", Toast.LENGTH_LONG).show();
            }
        });
    }

    private void setRecyclerView() {

//        recyclerView = (RecyclerView)findViewById(R.id.recyclerViewResult);
        recyclerView.setHasFixedSize(true);
        //   LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        //  recyclerView.setLayoutManager(layoutManager);//Linear Items
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getApplicationContext(), 3, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(gridLayoutManager);

        OtherPageAdapter adapter = new OtherPageAdapter(getApplicationContext(), arrayList, imageList);

        recyclerView.setAdapter(adapter);// set adapter on recyclerview
        recyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(getBaseContext(), recyclerView, new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
//                        Toast.makeText(getApplicationContext(),"Position :"+position,Toast.LENGTH_LONG).show();
//                        Toast.makeText(getApplicationContext(),"Link : "+arrayList.get(position),Toast.LENGTH_LONG).show();

                        Intent i = new Intent(MovieDetailActivity.this, MovieDetailActivity.class);
                        Bundle mBundle = new Bundle();
                        mBundle.putString("link", arrayList.get(position));
                        i.putExtras(mBundle);
                        startActivity(i);
                        finish();
                    }

                    @Override
                    public void onLongItemClick(View view, int position) {
                        // do whatever
                    }
                })
        );


        if (arrayList.size() == 0) {
            Toast.makeText(getApplicationContext(), "Sorry ! No Result Found.", Toast.LENGTH_LONG).show();
        }

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}
