package com.exclusive.fragments;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.fragment.app.Fragment;

import com.bumptech.glide.Glide;
import com.exclusive.R;
import com.exclusive.SplashActivity;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.util.Objects;

import de.hdodenhof.circleimageview.CircleImageView;

public class MenuFragment extends Fragment implements View.OnClickListener {
    private FirebaseAuth mAuth;
    private BottomSheetDialog bottomSheerDialog;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_menu, container, false);

        mAuth = FirebaseAuth.getInstance();

        CircleImageView circleImageView = v.findViewById(R.id.circleImageView);
        AppCompatTextView textName = v.findViewById(R.id.name);
        AppCompatTextView textEmail = v.findViewById(R.id.email);


        FirebaseUser user = mAuth.getCurrentUser();

        if (user != null) {
            Glide.with(this)
                    .load(user.getPhotoUrl())
                    .into(circleImageView);
        } else {
            Glide.with(this)
                    .load(R.drawable.account)
                    .into(circleImageView);
        }

        if (user != null) {
            textName.setText(user.getDisplayName());
        } else {
            textName.setText(getString(R.string.app_name));
        }
        if (user != null) {
            textEmail.setText(user.getEmail());
        } else {
            textName.setText(getString(R.string.app_name));
        }
        return v;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        LinearLayoutCompat logout = view.findViewById(R.id.logout);
        logout.setOnClickListener(this);

        LinearLayoutCompat about = view.findViewById(R.id.about);
        about.setOnClickListener(this);

        LinearLayoutCompat contactus = view.findViewById(R.id.contactus);
        contactus.setOnClickListener(this);

        LinearLayoutCompat tandc = view.findViewById(R.id.tandc);
        tandc.setOnClickListener(this);

        LinearLayoutCompat pp = view.findViewById(R.id.pp);
        pp.setOnClickListener(this);

        LinearLayoutCompat disclaimer = view.findViewById(R.id.disclaimer);
        disclaimer.setOnClickListener(this);

    }

    @Override
    public void onStart() {
        super.onStart();
        if (mAuth.getCurrentUser() == null) {
            getHome();
        }
    }

    private void getHome() {
        Objects.requireNonNull(getActivity()).finish();
        startActivity(new Intent(getActivity(), SplashActivity.class));
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.pp) {
            OpenDialogSheet(getString(R.string.privacy_policy), getString(R.string.privacy_policy_full));
        } else if (id == R.id.disclaimer) {
            OpenDialogSheet(getString(R.string.disclaimer), getString(R.string.disclaimer_full));
        } else if (id == R.id.tandc) {
            OpenDialogSheet(getString(R.string.terms_and_conditions), getString(R.string.terms_and_conditions_full));
        } else if (id == R.id.contactus) {
            OpenDialogSheet(getString(R.string.contactus), getString(R.string.contactus_full));
        } else if (id == R.id.about) {
            OpenDialogSheet(getString(R.string.about), getString(R.string.about_full));
        } else if (id == R.id.image) {
            bottomSheerDialog.dismiss();
        } else if (id == R.id.logout) {
            AlertDialog.Builder builder = new AlertDialog.Builder(Objects.requireNonNull(getActivity()));
            builder.setTitle("Logout Alert");
            builder.setMessage("You want to logout from this Application ?");
            builder.setPositiveButton("Logout",
                    (dialog, which) -> {
                        FirebaseAuth.getInstance().signOut();
                        getHome();

                    });
            builder.setNegativeButton(android.R.string.no, (dialog, which) -> {

            });
            builder.setCancelable(false);
            builder.show();
        }
    }

    private void OpenDialogSheet(String title, String description) {

        bottomSheerDialog = new BottomSheetDialog(Objects.requireNonNull(getActivity()));
        @SuppressLint("InflateParams") View parentView = getLayoutInflater().inflate(R.layout.bottomsheet, null);
        bottomSheerDialog.setContentView(parentView);

        AppCompatImageView imageView = parentView.findViewById(R.id.image);
        AppCompatTextView titles = parentView.findViewById(R.id.title);
        AppCompatTextView desc = parentView.findViewById(R.id.desc);

        titles.setText(title);
        desc.setText(description);
        imageView.setOnClickListener(this);

        bottomSheerDialog.show();
    }
}