package com.exclusive.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.exclusive.BuildConfig;
import com.exclusive.MoreByCategoryActivity;
import com.exclusive.MovieDetailActivity;
import com.exclusive.R;
import com.exclusive.WebSeriesDetailActivity;
import com.exclusive.adapter.HomepageAdapter;
import com.exclusive.apis.Api;
import com.exclusive.banner.AutoScrollViewPager;
import com.exclusive.banner.BannerAdapter;
import com.exclusive.models.PostsResponse;
import com.exclusive.utils.AppUpdateDialog;
import com.exclusive.utils.RecyclerItemClickListener;
import com.exclusive.utils.Utils;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.android.gms.tasks.Task;
import com.google.android.material.tabs.TabLayout;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class HomeFragment extends Fragment implements BannerAdapter.DetailsListener {

    private static final String FB_RC_KEY_TITLE = "update_title";
    private static final String FB_RC_KEY_DESCRIPTION = "update_description";
    private static final String FB_RC_KEY_FORCE_UPDATE_VERSION = "force_update_version";
    private static final String FB_RC_KEY_LATEST_VERSION = "latest_version";
    private static final int AUTO_SCROLL_THRESHOLD_IN_MILLI = 5000;

    final ArrayList<String> arrayList = new ArrayList<>();
    final OkHttpClient okHttpClient = new OkHttpClient.Builder()
            .readTimeout(5, TimeUnit.MINUTES)
            .connectTimeout(5, TimeUnit.MINUTES)
            .writeTimeout(5, TimeUnit.MINUTES)
            .build();
    AppUpdateDialog appUpdateDialog;
    FirebaseRemoteConfig mFirebaseRemoteConfig;
    ProgressBar progressBar;
    LinearLayoutCompat linearLayoutCompat;
    AutoScrollViewPager mViewPager;
    BannerAdapter bannerAdapter;
    List<String> BannerList = new ArrayList<>();
    List<String> BannerTypeList = new ArrayList<>();
    private ShimmerFrameLayout shimmer_view_latest;
    private ShimmerFrameLayout shimmer_view_popular;
    private ShimmerFrameLayout shimmer_view_action;
    private ShimmerFrameLayout shimmer_view_adventure;
    private ShimmerFrameLayout shimmer_view_drama;
    private ShimmerFrameLayout shimmer_view_sciencefiction;
    private ShimmerFrameLayout shimmer_view_bollywood;
    private ShimmerFrameLayout shimmer_view_crime;
    private ShimmerFrameLayout shimmer_view_comedy;
    private ShimmerFrameLayout shimmer_view_family;
    private ShimmerFrameLayout shimmer_view_disney;
    private ShimmerFrameLayout shimmer_view_romance;
    private ShimmerFrameLayout shimmer_view_kids;
    private ShimmerFrameLayout shimmer_view_mystery;
    private ShimmerFrameLayout shimmer_view_marvel;
    private ShimmerFrameLayout shimmer_view_hollywood;
    private ShimmerFrameLayout shimmer_view_fantasy;
    private ShimmerFrameLayout shimmer_view_horror;
    private ShimmerFrameLayout shimmer_view_sports;
    private ShimmerFrameLayout shimmer_view_history;
    private ShimmerFrameLayout shimmer_view_documentry;
    private ShimmerFrameLayout shimmer_view_war;
    private ShimmerFrameLayout shimmer_view_webseries;
    private ShimmerFrameLayout shimmer_view_banner;
    private RecyclerView rec_latestapi, rec_popularapi, rec_actionapi, rec_adventureapi, rec_drama,
            rec_sciencefiction, rec_bolly, rec_crime, rec_comedy, rec_family, rec_disney,
            rec_romance, rec_kids, rec_mystery, rec_marvel, rec_hollywood, rec_fantasy,
            rec_horror, rec_sports, rec_history, rec_documentry, rec_war, rec_webseries;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        checkAppUpdate();

        mViewPager = view.findViewById(R.id.viewPager);
        TabLayout tabs = view.findViewById(R.id.tabs);
        tabs.setupWithViewPager(mViewPager);

        mViewPager.startAutoScroll(AUTO_SCROLL_THRESHOLD_IN_MILLI);
        mViewPager.setInterval(AUTO_SCROLL_THRESHOLD_IN_MILLI);
        mViewPager.setCycle(true);

        shimmer_view_latest = view.findViewById(R.id.shimmer_view_latest);
        shimmer_view_latest.startShimmer();

        shimmer_view_popular = view.findViewById(R.id.shimmer_view_popular);
        shimmer_view_popular.startShimmer();

        shimmer_view_action = view.findViewById(R.id.shimmer_view_action);
        shimmer_view_action.startShimmer();

        shimmer_view_adventure = view.findViewById(R.id.shimmer_view_adventure);
        shimmer_view_adventure.startShimmer();

        shimmer_view_drama = view.findViewById(R.id.shimmer_view_drama);
        shimmer_view_drama.startShimmer();

        shimmer_view_sciencefiction = view.findViewById(R.id.shimmer_view_sciencefiction);
        shimmer_view_sciencefiction.startShimmer();

        shimmer_view_bollywood = view.findViewById(R.id.shimmer_view_bollywood);
        shimmer_view_bollywood.startShimmer();

        shimmer_view_crime = view.findViewById(R.id.shimmer_view_crime);
        shimmer_view_crime.startShimmer();

        shimmer_view_comedy = view.findViewById(R.id.shimmer_view_comedy);
        shimmer_view_comedy.startShimmer();

        shimmer_view_family = view.findViewById(R.id.shimmer_view_family);
        shimmer_view_family.startShimmer();

        shimmer_view_disney = view.findViewById(R.id.shimmer_view_disney);
        shimmer_view_disney.startShimmer();

        shimmer_view_romance = view.findViewById(R.id.shimmer_view_romance);
        shimmer_view_romance.startShimmer();

        shimmer_view_kids = view.findViewById(R.id.shimmer_view_kids);
        shimmer_view_kids.startShimmer();

        shimmer_view_mystery = view.findViewById(R.id.shimmer_view_mystery);
        shimmer_view_mystery.startShimmer();

        shimmer_view_marvel = view.findViewById(R.id.shimmer_view_marvel);
        shimmer_view_marvel.startShimmer();

        shimmer_view_hollywood = view.findViewById(R.id.shimmer_view_hollywood);
        shimmer_view_hollywood.startShimmer();

        shimmer_view_fantasy = view.findViewById(R.id.shimmer_view_fantasy);
        shimmer_view_fantasy.startShimmer();

        shimmer_view_horror = view.findViewById(R.id.shimmer_view_horror);
        shimmer_view_horror.startShimmer();

        shimmer_view_sports = view.findViewById(R.id.shimmer_view_sports);
        shimmer_view_sports.startShimmer();

        shimmer_view_history = view.findViewById(R.id.shimmer_view_history);
        shimmer_view_history.startShimmer();

        shimmer_view_documentry = view.findViewById(R.id.shimmer_view_documentry);
        shimmer_view_documentry.startShimmer();

        shimmer_view_war = view.findViewById(R.id.shimmer_view_war);
        shimmer_view_war.startShimmer();

        shimmer_view_webseries = view.findViewById(R.id.shimmer_view_webseries);
        shimmer_view_webseries.startShimmer();


        shimmer_view_banner = view.findViewById(R.id.shimmer_view_banner);
        shimmer_view_banner.startShimmer();


        progressBar = view.findViewById(R.id.progressBar);
        linearLayoutCompat = view.findViewById(R.id.linearLayoutCompat);
        linearLayoutCompat.setVisibility(View.GONE);
        rec_latestapi = view.findViewById(R.id.rec_latestapi);
        rec_popularapi = view.findViewById(R.id.rec_popularapi);
        rec_actionapi = view.findViewById(R.id.rec_actinapi);
        rec_adventureapi = view.findViewById(R.id.rec_adventureapi);
        rec_drama = view.findViewById(R.id.rec_drama);
        rec_sciencefiction = view.findViewById(R.id.rec_sciencefiction);
        rec_bolly = view.findViewById(R.id.rec_bolly);
        rec_crime = view.findViewById(R.id.rec_crime);
        rec_comedy = view.findViewById(R.id.rec_comedy);
        rec_family = view.findViewById(R.id.rec_family);
        rec_disney = view.findViewById(R.id.rec_disney);
        rec_romance = view.findViewById(R.id.rec_romance);
        rec_kids = view.findViewById(R.id.rec_kids);
        rec_mystery = view.findViewById(R.id.rec_mystery);
        rec_marvel = view.findViewById(R.id.rec_marvel);
        rec_hollywood = view.findViewById(R.id.rec_hollywood);
        rec_fantasy = view.findViewById(R.id.rec_fantasy);
        rec_horror = view.findViewById(R.id.rec_horror);
        rec_sports = view.findViewById(R.id.rec_sports);
        rec_history = view.findViewById(R.id.rec_history);
        rec_documentry = view.findViewById(R.id.rec_documentry);
        rec_war = view.findViewById(R.id.rec_war);
        rec_webseries = view.findViewById(R.id.rec_webseries);


        RelativeLayout latestapi_rl = view.findViewById(R.id.latestapi_rl);
        RelativeLayout popularapi_rl = view.findViewById(R.id.popularapi_rl);
        RelativeLayout actionapi_rl = view.findViewById(R.id.actionapi_rl);
        RelativeLayout adventureapi_rl = view.findViewById(R.id.adventureapi_rl);
        RelativeLayout drama_rl = view.findViewById(R.id.drama_rl);
        RelativeLayout sciencefiction_rl = view.findViewById(R.id.sciencefiction);
        RelativeLayout bollywood = view.findViewById(R.id.bollywood);
        RelativeLayout crime = view.findViewById(R.id.crime);
        RelativeLayout comedy = view.findViewById(R.id.comedy);
        RelativeLayout family = view.findViewById(R.id.family);
        RelativeLayout dinsey = view.findViewById(R.id.disney);
        RelativeLayout romance = view.findViewById(R.id.romance);
        RelativeLayout kids = view.findViewById(R.id.kids);
        RelativeLayout mystery = view.findViewById(R.id.mystery);
        RelativeLayout marvel = view.findViewById(R.id.marvel);
        RelativeLayout hollywood = view.findViewById(R.id.hollywood);
        RelativeLayout fantasy = view.findViewById(R.id.fantasy);
        RelativeLayout horror = view.findViewById(R.id.horror);
        RelativeLayout sports = view.findViewById(R.id.sports);
        RelativeLayout history = view.findViewById(R.id.history);
        RelativeLayout documentry = view.findViewById(R.id.documentry);
        RelativeLayout war = view.findViewById(R.id.war);
        RelativeLayout webseries = view.findViewById(R.id.webseries);


        latestapi_rl.setOnClickListener(view1 -> goToNext("Latest", "17"));
        popularapi_rl.setOnClickListener(view14 -> goToNext("Popular", "16"));
        actionapi_rl.setOnClickListener(view12 -> goToNext("Action", "5"));
        adventureapi_rl.setOnClickListener(view13 -> goToNext("Adventure", "19"));
        drama_rl.setOnClickListener(view15 -> goToNext("Drama", "36"));
        sciencefiction_rl.setOnClickListener(view16 -> goToNext("Science Fiction", "18"));
        bollywood.setOnClickListener(view17 -> goToNext("Bollywood", "2"));
        crime.setOnClickListener(view18 -> goToNext("Crime", "35"));
        comedy.setOnClickListener(view19 -> goToNext("Comedy", "7"));
        family.setOnClickListener(view110 -> goToNext("Family", "3"));
        dinsey.setOnClickListener(view111 -> goToNext("Disney", "20"));
        romance.setOnClickListener(view111 -> goToNext("Romance", "4"));
        kids.setOnClickListener(view111 -> goToNext("Kids", "26"));
        mystery.setOnClickListener(view111 -> goToNext("Mystery", "39"));
        marvel.setOnClickListener(view111 -> goToNext("Marvel", "8"));
        hollywood.setOnClickListener(view111 -> goToNext("Hollywood", "14"));
        fantasy.setOnClickListener(view111 -> goToNext("Fantasy", "38"));
        horror.setOnClickListener(view111 -> goToNext("Horror", "31"));
        sports.setOnClickListener(view111 -> goToNext("Sports", "40"));
        history.setOnClickListener(view111 -> goToNext("History", "37"));
        documentry.setOnClickListener(view111 -> goToNext("Documentary", "42"));
        war.setOnClickListener(view111 -> goToNext("War and Military", "41"));
        webseries.setOnClickListener(view111 -> goToNextWebSeries("Web Series", "59"));


        progressBar.setVisibility(View.VISIBLE);

        requireActivity().runOnUiThread(this::Get_banner);
        getActivity().runOnUiThread(() -> {
            setImage();
            setImage1();
            setImage2();
            setImage3();
        });

        getActivity().runOnUiThread(() -> {
            loadData("59");
        });

        getActivity().runOnUiThread(() -> {
            loadData("17");
            loadData("18");
            loadData("2");
            loadData("35");
            loadData("7");
            loadData("2");
            loadData("3");
            loadData("20");
            loadData("4");
        });
        getActivity().runOnUiThread(() -> {
            loadData("26");
            loadData("39");
            loadData("8");
            loadData("14");
            loadData("38");
            loadData("31");
            loadData("40");
        });
        getActivity().runOnUiThread(() -> {
            loadData("37");
            loadData("42");
            loadData("41");
        });


        return view;
    }


    private void goToNext(String title, String id) {
        Intent i = new Intent(getActivity(), MoreByCategoryActivity.class);
        i.putExtra("title", title);
        i.putExtra("type", "movies");
        i.putExtra("id", id);
        startActivity(i);
    }

    private void goToNextWebSeries(String title, String id) {
        Intent i = new Intent(getActivity(), MoreByCategoryActivity.class);
        i.putExtra("title", title);
        i.putExtra("type", "web_series");
        i.putExtra("id", id);
        startActivity(i);
    }

    private void Get_banner() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Api.BASE_URL)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        Api api = retrofit.create(Api.class);
        Call<List<PostsResponse>> call;
        Map<String, String> data = Utils.getTop5();
        data.put("categories", "43");
        call = api.postsByMultiCateegory100(data);
        call.enqueue(new Callback<List<PostsResponse>>() {
            @Override
            public void onResponse(@NotNull Call<List<PostsResponse>> call, @NotNull Response<List<PostsResponse>> response) {
                if (response.body() != null && response.body().size() != 0) {
                    String dataOfBanner = response.body().get(0).getContent().getRendered();
                    Log.e("data_of_web", "series => " + dataOfBanner);
                    Log.e("data_of_web", "----------------------------------");
                    dataOfBanner = stripHtml(dataOfBanner);

                    JSONObject jsonObject = new JSONObject();
                    try {
                        dataOfBanner = dataOfBanner.replaceAll("″", "\"");
                        dataOfBanner = dataOfBanner.replaceAll("”", "\"");
                        dataOfBanner = dataOfBanner.replaceAll("“", "\"");
                        jsonObject = new JSONObject(dataOfBanner);
                        JSONArray jsonArray = jsonObject.optJSONArray("banner");
                        if (jsonArray != null) {
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                                String image_url = jsonObject1.getString("image_url");
                                String post_id = jsonObject1.getString("post_id");
                                String post_type = jsonObject1.getString("post_type");
                                BannerList.add("" + image_url);
                                BannerTypeList.add("" + post_type);
                                arrayList.add("" + post_id);
                            }
                            SetBanner();
                        }
                    } catch (Exception error) {
                        Log.e("Exception", "series => " + error, error);
                    }
                }
            }


            @Override
            public void onFailure(@NotNull Call<List<PostsResponse>> call, @NotNull Throwable t) {
                SetBanner();
            }
        });
    }

    public String stripHtml(String html) {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            return Html.fromHtml(html, Html.FROM_HTML_MODE_LEGACY).toString();
        } else {
            return Html.fromHtml(html).toString();
        }
    }

    public void SetBanner() {
        bannerAdapter = new BannerAdapter(getActivity(), BannerList, this);
        mViewPager.setAdapter(bannerAdapter);

        shimmer_view_banner.stopShimmer();
        shimmer_view_banner.setVisibility(View.GONE);
    }

    private void setImage() {
        final ArrayList<String> arrayList = new ArrayList<>();
        final ArrayList<String> imageList = new ArrayList<>();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Api.BASE_URL)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create()) //Here we are using the GsonConverterFactory to directly convert json data to object
                .build();

        Api api = retrofit.create(Api.class);
        Call<List<PostsResponse>> call;
        Map<String, String> data = Utils.getOptions();
        data.put("categories", "17");
        call = api.postsByMultiCateegory100(data);
        call.enqueue(new Callback<List<PostsResponse>>() {
            @Override
            public void onResponse(@NotNull Call<List<PostsResponse>> call, @NotNull Response<List<PostsResponse>> response) {
                if (response.body() != null && response.body().size() != 0) {
                    for (int i = 0; i < response.body().size(); i++) {
                        arrayList.add("" + response.body().get(i).getId());
                        imageList.add("" + (response.body().get(i).getJetpackFeaturedMediaUrl()));
                    }
                }

                rec_latestapi.setHasFixedSize(true);
                rec_latestapi.setNestedScrollingEnabled(false);

                rec_latestapi.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));


                HomepageAdapter adapter = new HomepageAdapter(getActivity(), arrayList, imageList);

                rec_latestapi.setAdapter(adapter);

                Objects.requireNonNull(rec_latestapi.getLayoutManager()).scrollToPosition(0);

                rec_latestapi.addOnItemTouchListener(
                        new RecyclerItemClickListener(getActivity(), rec_latestapi, new RecyclerItemClickListener.OnItemClickListener() {
                            @Override
                            public void onItemClick(View view, int position) {
                                Intent i = new Intent(getActivity(), MovieDetailActivity.class);
                                Bundle mBundle = new Bundle();
                                mBundle.putString("link", arrayList.get(position));
                                i.putExtras(mBundle);
                                startActivity(i);
                            }

                            @Override
                            public void onLongItemClick(View view, int position) {
                                // do whatever
                            }
                        })
                );


                try {
                    rec_latestapi.setVisibility(View.VISIBLE);
                    shimmer_view_latest.stopShimmer();
                    shimmer_view_latest.setVisibility(View.GONE);
                } catch (Exception ignored) {
                }


            }

            @Override
            public void onFailure(@NotNull Call<List<PostsResponse>> call, @NotNull Throwable t) {
                t.printStackTrace();

            }
        });
    }

    private void setImage1() {
        final ArrayList<String> arrayList = new ArrayList<>();
        final ArrayList<String> imageList = new ArrayList<>();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Api.BASE_URL)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create()) //Here we are using the GsonConverterFactory to directly convert json data to object
                .build();

        Api api = retrofit.create(Api.class);
        Call<List<PostsResponse>> call;
        Map<String, String> data = Utils.getOptions();
        data.put("categories", "16");
        call = api.postsByMultiCateegory100(data);
        call.enqueue(new Callback<List<PostsResponse>>() {
            @Override
            public void onResponse(@NotNull Call<List<PostsResponse>> call, @NotNull Response<List<PostsResponse>> response) {


                if (response.body() != null && response.body().size() != 0) {
                    for (int i = 0; i < response.body().size(); i++) {
                        arrayList.add("" + response.body().get(i).getId());
                        imageList.add("" + (response.body().get(i).getJetpackFeaturedMediaUrl()));
                    }
                }

                rec_popularapi.setHasFixedSize(true);
                rec_popularapi.setNestedScrollingEnabled(false);
                rec_popularapi.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));


                HomepageAdapter adapter = new HomepageAdapter(getActivity(), arrayList, imageList);

                rec_popularapi.setAdapter(adapter);
                Objects.requireNonNull(rec_popularapi.getLayoutManager()).scrollToPosition(0);

                rec_popularapi.addOnItemTouchListener(
                        new RecyclerItemClickListener(getActivity(), rec_popularapi, new RecyclerItemClickListener.OnItemClickListener() {
                            @Override
                            public void onItemClick(View view, int position) {
                                Intent i = new Intent(getActivity(), MovieDetailActivity.class);
                                Bundle mBundle = new Bundle();
                                mBundle.putString("link", arrayList.get(position));
                                i.putExtras(mBundle);
                                startActivity(i);
                            }

                            @Override
                            public void onLongItemClick(View view, int position) {

                            }
                        })
                );


                try {
                    rec_popularapi.setVisibility(View.VISIBLE);
                    shimmer_view_popular.stopShimmer();
                    shimmer_view_popular.setVisibility(View.GONE);
                } catch (Exception ignored) {
                }


            }

            @Override
            public void onFailure(@NotNull Call<List<PostsResponse>> call, @NotNull Throwable t) {

                t.printStackTrace();
            }
        });
    }

    private void setImage2() {
        final ArrayList<String> arrayList = new ArrayList<>();
        final ArrayList<String> imageList = new ArrayList<>();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Api.BASE_URL)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create()) //Here we are using the GsonConverterFactory to directly convert json data to object
                .build();

        Api api = retrofit.create(Api.class);
        Call<List<PostsResponse>> call;
        Map<String, String> data = Utils.getOptions();
        data.put("categories", "18");
        call = api.postsByMultiCateegory100(data);
        call.enqueue(new Callback<List<PostsResponse>>() {
            @Override
            public void onResponse(@NotNull Call<List<PostsResponse>> call, @NotNull Response<List<PostsResponse>> response) {


                if (response.body() != null && response.body().size() != 0) {
                    for (int i = 0; i < response.body().size(); i++) {
                        arrayList.add("" + response.body().get(i).getId());
                        imageList.add("" + (response.body().get(i).getJetpackFeaturedMediaUrl()));
                    }
                }

                rec_actionapi.setHasFixedSize(true);
                rec_actionapi.setNestedScrollingEnabled(false);

                rec_actionapi.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));


                HomepageAdapter adapter = new HomepageAdapter(getActivity(), arrayList, imageList);

                rec_actionapi.setAdapter(adapter);

                Objects.requireNonNull(rec_actionapi.getLayoutManager()).scrollToPosition(0);

                rec_actionapi.addOnItemTouchListener(
                        new RecyclerItemClickListener(getActivity(), rec_actionapi, new RecyclerItemClickListener.OnItemClickListener() {
                            @Override
                            public void onItemClick(View view, int position) {
                                Intent i = new Intent(getActivity(), MovieDetailActivity.class);
                                Bundle mBundle = new Bundle();
                                mBundle.putString("link", arrayList.get(position));
                                i.putExtras(mBundle);
                                startActivity(i);
                            }

                            @Override
                            public void onLongItemClick(View view, int position) {
                                // do whatever
                            }
                        })
                );


                try {
                    rec_actionapi.setVisibility(View.VISIBLE);
                    shimmer_view_action.stopShimmer();
                    shimmer_view_action.setVisibility(View.GONE);
                } catch (Exception ignored) {
                }


            }

            @Override
            public void onFailure(@NotNull Call<List<PostsResponse>> call, @NotNull Throwable t) {
                t.printStackTrace();

            }
        });
    }

    private void setImage3() {
        final ArrayList<String> arrayList = new ArrayList<>();
        final ArrayList<String> imageList = new ArrayList<>();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Api.BASE_URL)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create()) //Here we are using the GsonConverterFactory to directly convert json data to object
                .build();

        Api api = retrofit.create(Api.class);
        Call<List<PostsResponse>> call;
        Map<String, String> data = Utils.getOptions();
        data.put("categories", "19");
        call = api.postsByMultiCateegory100(data);
        call.enqueue(new Callback<List<PostsResponse>>() {
            @Override
            public void onResponse(@NotNull Call<List<PostsResponse>> call, @NotNull Response<List<PostsResponse>> response) {
                progressBar.setVisibility(View.GONE);
                linearLayoutCompat.setVisibility(View.VISIBLE);


                if (response.body() != null && response.body().size() != 0) {
                    for (int i = 0; i < response.body().size(); i++) {
                        arrayList.add("" + response.body().get(i).getId());
                        imageList.add("" + (response.body().get(i).getJetpackFeaturedMediaUrl()));
                    }
                }

                rec_adventureapi.setHasFixedSize(true);
                rec_adventureapi.setNestedScrollingEnabled(false);
                rec_adventureapi.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));


                HomepageAdapter adapter = new HomepageAdapter(getActivity(), arrayList, imageList);

                rec_adventureapi.setAdapter(adapter);
                // bannerIntialize();
                Objects.requireNonNull(rec_adventureapi.getLayoutManager()).scrollToPosition(0);

                rec_adventureapi.addOnItemTouchListener(
                        new RecyclerItemClickListener(getActivity(), rec_adventureapi, new RecyclerItemClickListener.OnItemClickListener() {
                            @Override
                            public void onItemClick(View view, int position) {
                                Intent i = new Intent(getActivity(), MovieDetailActivity.class);
                                Bundle mBundle = new Bundle();
                                mBundle.putString("link", arrayList.get(position));
                                i.putExtras(mBundle);
                                startActivity(i);
                            }

                            @Override
                            public void onLongItemClick(View view, int position) {
                                // do whatever
                            }
                        })
                );


                try {
                    rec_adventureapi.setVisibility(View.VISIBLE);
                    shimmer_view_adventure.stopShimmer();
                    shimmer_view_adventure.setVisibility(View.GONE);
                } catch (Exception ignored) {
                }


            }

            @Override
            public void onFailure(@NotNull Call<List<PostsResponse>> call, @NotNull Throwable t) {
                t.printStackTrace();
                //loading(false);
                progressBar.setVisibility(View.GONE);
                linearLayoutCompat.setVisibility(View.VISIBLE);
            }
        });
    }

    private void loadData(final String id) {
        progressBar.setVisibility(View.VISIBLE);

        final ArrayList<String> arrayList = new ArrayList<>();
        final ArrayList<String> imageList = new ArrayList<>();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Api.BASE_URL)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create()) //Here we are using the GsonConverterFactory to directly convert json data to object
                .build();

        Api api = retrofit.create(Api.class);
        Call<List<PostsResponse>> call;
        Map<String, String> data = Utils.getOptions();
        data.put("categories", id);
        call = api.postsByMultiCateegory100(data);
        call.enqueue(new Callback<List<PostsResponse>>() {
            @Override
            public void onResponse(@NotNull Call<List<PostsResponse>> call, @NotNull Response<List<PostsResponse>> response) {
                progressBar.setVisibility(View.GONE);
                linearLayoutCompat.setVisibility(View.VISIBLE);
                if (response.body() != null && response.body().size() != 0) {
                    for (int i = 0; i < response.body().size(); i++) {
                        arrayList.add("" + response.body().get(i).getId());
                        imageList.add("" + (response.body().get(i).getJetpackFeaturedMediaUrl()));
                        // if(i<=4)
                        //    bannerList.add(new Banner(response.body().get(i).getJetpackFeaturedMediaUrl(),getBackgourndImageUrl(response.body().get(i).getContent().getRendered()),response.body().get(i).getTitle().getRendered(),response.body().get(i).getId()));
                    }
                }

                if (id.compareTo("17") == 0) {
                    rec_drama.setHasFixedSize(true);
                    rec_drama.setNestedScrollingEnabled(false);

                    rec_drama.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));


                    HomepageAdapter adapter = new HomepageAdapter(getActivity(), arrayList, imageList);

                    rec_drama.setAdapter(adapter);
                    // bannerIntialize();
                    Objects.requireNonNull(rec_drama.getLayoutManager()).scrollToPosition(0);

                    rec_drama.addOnItemTouchListener(
                            new RecyclerItemClickListener(getActivity(), rec_drama, new RecyclerItemClickListener.OnItemClickListener() {
                                @Override
                                public void onItemClick(View view, int position) {
                                    Intent i = new Intent(getActivity(), MovieDetailActivity.class);
                                    Bundle mBundle = new Bundle();
                                    mBundle.putString("link", arrayList.get(position));
                                    i.putExtras(mBundle);
                                    startActivity(i);
                                }

                                @Override
                                public void onLongItemClick(View view, int position) {
                                    // do whatever
                                }
                            })
                    );
                    try {
                        rec_drama.setVisibility(View.VISIBLE);
                        shimmer_view_drama.stopShimmer();
                        shimmer_view_drama.setVisibility(View.GONE);
                    } catch (Exception ignored) {
                    }

                } else if (id.compareTo("18") == 0) {
                    rec_sciencefiction.setHasFixedSize(true);
                    rec_sciencefiction.setNestedScrollingEnabled(false);
                    rec_sciencefiction.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
                    HomepageAdapter adapter = new HomepageAdapter(getActivity(), arrayList, imageList);

                    rec_sciencefiction.setAdapter(adapter);
                    // bannerIntialize();
                    Objects.requireNonNull(rec_sciencefiction.getLayoutManager()).scrollToPosition(0);

                    rec_sciencefiction.addOnItemTouchListener(
                            new RecyclerItemClickListener(getActivity(), rec_sciencefiction, new RecyclerItemClickListener.OnItemClickListener() {
                                @Override
                                public void onItemClick(View view, int position) {
                                    Intent i = new Intent(getActivity(), MovieDetailActivity.class);
                                    Bundle mBundle = new Bundle();
                                    mBundle.putString("link", arrayList.get(position));
                                    i.putExtras(mBundle);
                                    startActivity(i);
                                }

                                @Override
                                public void onLongItemClick(View view, int position) {
                                    // do whatever
                                }
                            })
                    );
                    try {
                        rec_sciencefiction.setVisibility(View.VISIBLE);
                        shimmer_view_sciencefiction.stopShimmer();
                        shimmer_view_sciencefiction.setVisibility(View.GONE);
                    } catch (Exception ignored) {
                    }
                } else if (id.compareTo("2") == 0) {
                    rec_bolly.setHasFixedSize(true);
                    rec_bolly.setNestedScrollingEnabled(false);

                    rec_bolly.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));


                    HomepageAdapter adapter = new HomepageAdapter(getActivity(), arrayList, imageList);

                    rec_bolly.setAdapter(adapter);
                    // bannerIntialize();
                    Objects.requireNonNull(rec_bolly.getLayoutManager()).scrollToPosition(0);

                    rec_bolly.addOnItemTouchListener(
                            new RecyclerItemClickListener(getActivity(), rec_bolly, new RecyclerItemClickListener.OnItemClickListener() {
                                @Override
                                public void onItemClick(View view, int position) {
                                    Intent i = new Intent(getActivity(), MovieDetailActivity.class);
                                    Bundle mBundle = new Bundle();
                                    mBundle.putString("link", arrayList.get(position));
                                    i.putExtras(mBundle);
                                    startActivity(i);
                                }

                                @Override
                                public void onLongItemClick(View view, int position) {
                                    // do whatever
                                }
                            })
                    );
                    try {
                        rec_bolly.setVisibility(View.VISIBLE);
                        shimmer_view_bollywood.stopShimmer();
                        shimmer_view_bollywood.setVisibility(View.GONE);
                    } catch (Exception ignored) {
                    }

                } else if (id.compareTo("35") == 0) {
                    rec_crime.setHasFixedSize(true);
                    rec_crime.setNestedScrollingEnabled(false);

                    rec_crime.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));


                    HomepageAdapter adapter = new HomepageAdapter(getActivity(), arrayList, imageList);

                    rec_crime.setAdapter(adapter);
                    // bannerIntialize();
                    Objects.requireNonNull(rec_crime.getLayoutManager()).scrollToPosition(0);

                    rec_crime.addOnItemTouchListener(
                            new RecyclerItemClickListener(getActivity(), rec_crime, new RecyclerItemClickListener.OnItemClickListener() {
                                @Override
                                public void onItemClick(View view, int position) {
                                    Intent i = new Intent(getActivity(), MovieDetailActivity.class);
                                    Bundle mBundle = new Bundle();
                                    mBundle.putString("link", arrayList.get(position));
                                    i.putExtras(mBundle);
                                    startActivity(i);
                                }

                                @Override
                                public void onLongItemClick(View view, int position) {
                                    // do whatever
                                }
                            })
                    );
                    try {
                        rec_crime.setVisibility(View.VISIBLE);
                        shimmer_view_crime.stopShimmer();
                        shimmer_view_crime.setVisibility(View.GONE);
                    } catch (Exception ignored) {
                    }

                } else if (id.compareTo("7") == 0) {
                    rec_comedy.setHasFixedSize(true);
                    rec_comedy.setNestedScrollingEnabled(false);

                    rec_comedy.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));


                    HomepageAdapter adapter = new HomepageAdapter(getActivity(), arrayList, imageList);

                    rec_comedy.setAdapter(adapter);
                    // bannerIntialize();
                    Objects.requireNonNull(rec_comedy.getLayoutManager()).scrollToPosition(0);

                    rec_comedy.addOnItemTouchListener(
                            new RecyclerItemClickListener(getActivity(), rec_comedy, new RecyclerItemClickListener.OnItemClickListener() {
                                @Override
                                public void onItemClick(View view, int position) {
                                    Intent i = new Intent(getActivity(), MovieDetailActivity.class);
                                    Bundle mBundle = new Bundle();
                                    mBundle.putString("link", arrayList.get(position));
                                    i.putExtras(mBundle);
                                    startActivity(i);
                                }

                                @Override
                                public void onLongItemClick(View view, int position) {
                                    // do whatever
                                }
                            })
                    );

                    try {
                        rec_comedy.setVisibility(View.VISIBLE);
                        shimmer_view_comedy.stopShimmer();
                        shimmer_view_comedy.setVisibility(View.GONE);
                    } catch (Exception ignored) {
                    }
                } else if (id.compareTo("3") == 0) {
                    rec_family.setHasFixedSize(true);
                    rec_family.setNestedScrollingEnabled(false);

                    rec_family.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));


                    HomepageAdapter adapter = new HomepageAdapter(getActivity(), arrayList, imageList);

                    rec_family.setAdapter(adapter);
                    // bannerIntialize();
                    Objects.requireNonNull(rec_family.getLayoutManager()).scrollToPosition(0);

                    rec_family.addOnItemTouchListener(
                            new RecyclerItemClickListener(getActivity(), rec_family, new RecyclerItemClickListener.OnItemClickListener() {
                                @Override
                                public void onItemClick(View view, int position) {
                                    Intent i = new Intent(getActivity(), MovieDetailActivity.class);
                                    Bundle mBundle = new Bundle();
                                    mBundle.putString("link", arrayList.get(position));
                                    i.putExtras(mBundle);
                                    startActivity(i);
                                }

                                @Override
                                public void onLongItemClick(View view, int position) {
                                    // do whatever
                                }
                            })
                    );
                    try {
                        rec_family.setVisibility(View.VISIBLE);
                        shimmer_view_family.stopShimmer();
                        shimmer_view_family.setVisibility(View.GONE);
                    } catch (Exception ignored) {
                    }
                } else if (id.compareTo("20") == 0) {
                    rec_disney.setHasFixedSize(true);
                    rec_disney.setNestedScrollingEnabled(false);

                    rec_disney.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));


                    HomepageAdapter adapter = new HomepageAdapter(getActivity(), arrayList, imageList);

                    rec_disney.setAdapter(adapter);
                    // bannerIntialize();
                    Objects.requireNonNull(rec_disney.getLayoutManager()).scrollToPosition(0);

                    rec_disney.addOnItemTouchListener(
                            new RecyclerItemClickListener(getActivity(), rec_disney, new RecyclerItemClickListener.OnItemClickListener() {
                                @Override
                                public void onItemClick(View view, int position) {
                                    Intent i = new Intent(getActivity(), MovieDetailActivity.class);
                                    Bundle mBundle = new Bundle();
                                    mBundle.putString("link", arrayList.get(position));
                                    i.putExtras(mBundle);
                                    startActivity(i);
                                }

                                @Override
                                public void onLongItemClick(View view, int position) {
                                    // do whatever
                                }
                            })
                    );
                    try {
                        rec_disney.setVisibility(View.VISIBLE);
                        shimmer_view_disney.stopShimmer();
                        shimmer_view_disney.setVisibility(View.GONE);
                    } catch (Exception ignored) {
                    }
                } else if (id.compareTo("4") == 0) {
                    rec_romance.setHasFixedSize(true);
                    rec_romance.setNestedScrollingEnabled(false);

                    rec_romance.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));


                    HomepageAdapter adapter = new HomepageAdapter(getActivity(), arrayList, imageList);

                    rec_romance.setAdapter(adapter);
                    // bannerIntialize();
                    Objects.requireNonNull(rec_romance.getLayoutManager()).scrollToPosition(0);

                    rec_romance.addOnItemTouchListener(
                            new RecyclerItemClickListener(getActivity(), rec_romance, new RecyclerItemClickListener.OnItemClickListener() {
                                @Override
                                public void onItemClick(View view, int position) {
                                    Intent i = new Intent(getActivity(), MovieDetailActivity.class);
                                    Bundle mBundle = new Bundle();
                                    mBundle.putString("link", arrayList.get(position));
                                    i.putExtras(mBundle);
                                    startActivity(i);
                                }

                                @Override
                                public void onLongItemClick(View view, int position) {
                                    // do whatever
                                }
                            })
                    );
                    try {
                        rec_romance.setVisibility(View.VISIBLE);
                        shimmer_view_romance.stopShimmer();
                        shimmer_view_romance.setVisibility(View.GONE);
                    } catch (Exception ignored) {
                    }
                } else if (id.compareTo("26") == 0) {
                    rec_kids.setHasFixedSize(true);
                    rec_kids.setNestedScrollingEnabled(false);

                    rec_kids.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));


                    HomepageAdapter adapter = new HomepageAdapter(getActivity(), arrayList, imageList);

                    rec_kids.setAdapter(adapter);
                    // bannerIntialize();
                    Objects.requireNonNull(rec_kids.getLayoutManager()).scrollToPosition(0);

                    rec_kids.addOnItemTouchListener(
                            new RecyclerItemClickListener(getActivity(), rec_kids, new RecyclerItemClickListener.OnItemClickListener() {
                                @Override
                                public void onItemClick(View view, int position) {
                                    Intent i = new Intent(getActivity(), MovieDetailActivity.class);
                                    Bundle mBundle = new Bundle();
                                    mBundle.putString("link", arrayList.get(position));
                                    i.putExtras(mBundle);
                                    startActivity(i);
                                }

                                @Override
                                public void onLongItemClick(View view, int position) {
                                    // do whatever
                                }
                            })
                    );
                    try {
                        rec_kids.setVisibility(View.VISIBLE);
                        shimmer_view_kids.stopShimmer();
                        shimmer_view_kids.setVisibility(View.GONE);
                    } catch (Exception ignored) {
                    }
                } else if (id.compareTo("39") == 0) {
                    rec_mystery.setHasFixedSize(true);
                    rec_mystery.setNestedScrollingEnabled(false);

                    rec_mystery.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));


                    HomepageAdapter adapter = new HomepageAdapter(getActivity(), arrayList, imageList);

                    rec_mystery.setAdapter(adapter);
                    // bannerIntialize();
                    Objects.requireNonNull(rec_mystery.getLayoutManager()).scrollToPosition(0);

                    rec_mystery.addOnItemTouchListener(
                            new RecyclerItemClickListener(getActivity(), rec_mystery, new RecyclerItemClickListener.OnItemClickListener() {
                                @Override
                                public void onItemClick(View view, int position) {
                                    Intent i = new Intent(getActivity(), MovieDetailActivity.class);
                                    Bundle mBundle = new Bundle();
                                    mBundle.putString("link", arrayList.get(position));
                                    i.putExtras(mBundle);
                                    startActivity(i);
                                }

                                @Override
                                public void onLongItemClick(View view, int position) {
                                    // do whatever
                                }
                            })
                    );
                    try {
                        rec_mystery.setVisibility(View.VISIBLE);
                        shimmer_view_mystery.stopShimmer();
                        shimmer_view_mystery.setVisibility(View.GONE);
                    } catch (Exception ignored) {
                    }
                } else if (id.compareTo("8") == 0) {
                    rec_marvel.setHasFixedSize(true);
                    rec_marvel.setNestedScrollingEnabled(false);

                    rec_marvel.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));


                    HomepageAdapter adapter = new HomepageAdapter(getActivity(), arrayList, imageList);

                    rec_marvel.setAdapter(adapter);
                    // bannerIntialize();
                    Objects.requireNonNull(rec_marvel.getLayoutManager()).scrollToPosition(0);

                    rec_marvel.addOnItemTouchListener(
                            new RecyclerItemClickListener(getActivity(), rec_marvel, new RecyclerItemClickListener.OnItemClickListener() {
                                @Override
                                public void onItemClick(View view, int position) {
                                    Intent i = new Intent(getActivity(), MovieDetailActivity.class);
                                    Bundle mBundle = new Bundle();
                                    mBundle.putString("link", arrayList.get(position));
                                    i.putExtras(mBundle);
                                    startActivity(i);
                                }

                                @Override
                                public void onLongItemClick(View view, int position) {
                                    // do whatever
                                }
                            })
                    );
                    try {
                        rec_marvel.setVisibility(View.VISIBLE);
                        shimmer_view_marvel.stopShimmer();
                        shimmer_view_marvel.setVisibility(View.GONE);
                    } catch (Exception ignored) {
                    }
                } else if (id.compareTo("14") == 0) {
                    rec_hollywood.setHasFixedSize(true);
                    rec_hollywood.setNestedScrollingEnabled(false);

                    rec_hollywood.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));


                    HomepageAdapter adapter = new HomepageAdapter(getActivity(), arrayList, imageList);

                    rec_hollywood.setAdapter(adapter);
                    // bannerIntialize();
                    Objects.requireNonNull(rec_hollywood.getLayoutManager()).scrollToPosition(0);

                    rec_hollywood.addOnItemTouchListener(
                            new RecyclerItemClickListener(getActivity(), rec_hollywood, new RecyclerItemClickListener.OnItemClickListener() {
                                @Override
                                public void onItemClick(View view, int position) {
                                    Intent i = new Intent(getActivity(), MovieDetailActivity.class);
                                    Bundle mBundle = new Bundle();
                                    mBundle.putString("link", arrayList.get(position));
                                    i.putExtras(mBundle);
                                    startActivity(i);
                                }

                                @Override
                                public void onLongItemClick(View view, int position) {
                                    // do whatever
                                }
                            })
                    );
                    try {
                        rec_hollywood.setVisibility(View.VISIBLE);
                        shimmer_view_hollywood.stopShimmer();
                        shimmer_view_hollywood.setVisibility(View.GONE);
                    } catch (Exception ignored) {
                    }
                } else if (id.compareTo("38") == 0) {
                    rec_fantasy.setHasFixedSize(true);
                    rec_fantasy.setNestedScrollingEnabled(false);

                    rec_fantasy.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));


                    HomepageAdapter adapter = new HomepageAdapter(getActivity(), arrayList, imageList);

                    rec_fantasy.setAdapter(adapter);
                    // bannerIntialize();
                    Objects.requireNonNull(rec_fantasy.getLayoutManager()).scrollToPosition(0);

                    rec_fantasy.addOnItemTouchListener(
                            new RecyclerItemClickListener(getActivity(), rec_fantasy, new RecyclerItemClickListener.OnItemClickListener() {
                                @Override
                                public void onItemClick(View view, int position) {
                                    Intent i = new Intent(getActivity(), MovieDetailActivity.class);
                                    Bundle mBundle = new Bundle();
                                    mBundle.putString("link", arrayList.get(position));
                                    i.putExtras(mBundle);
                                    startActivity(i);
                                }

                                @Override
                                public void onLongItemClick(View view, int position) {
                                    // do whatever
                                }
                            })
                    );
                    try {
                        rec_fantasy.setVisibility(View.VISIBLE);
                        shimmer_view_fantasy.stopShimmer();
                        shimmer_view_fantasy.setVisibility(View.GONE);
                    } catch (Exception ignored) {
                    }
                } else if (id.compareTo("31") == 0) {
                    rec_horror.setHasFixedSize(true);
                    rec_horror.setNestedScrollingEnabled(false);

                    rec_horror.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));


                    HomepageAdapter adapter = new HomepageAdapter(getActivity(), arrayList, imageList);

                    rec_horror.setAdapter(adapter);
                    // bannerIntialize();
                    Objects.requireNonNull(rec_horror.getLayoutManager()).scrollToPosition(0);

                    rec_horror.addOnItemTouchListener(
                            new RecyclerItemClickListener(getActivity(), rec_horror, new RecyclerItemClickListener.OnItemClickListener() {
                                @Override
                                public void onItemClick(View view, int position) {
                                    Intent i = new Intent(getActivity(), MovieDetailActivity.class);
                                    Bundle mBundle = new Bundle();
                                    mBundle.putString("link", arrayList.get(position));
                                    i.putExtras(mBundle);
                                    startActivity(i);
                                }

                                @Override
                                public void onLongItemClick(View view, int position) {
                                    // do whatever
                                }
                            })
                    );
                    try {
                        rec_horror.setVisibility(View.VISIBLE);
                        shimmer_view_horror.stopShimmer();
                        shimmer_view_horror.setVisibility(View.GONE);
                    } catch (Exception ignored) {
                    }
                } else if (id.compareTo("40") == 0) {
                    rec_sports.setHasFixedSize(true);
                    rec_sports.setNestedScrollingEnabled(false);

                    rec_sports.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));


                    HomepageAdapter adapter = new HomepageAdapter(getActivity(), arrayList, imageList);

                    rec_sports.setAdapter(adapter);
                    // bannerIntialize();
                    Objects.requireNonNull(rec_sports.getLayoutManager()).scrollToPosition(0);

                    rec_sports.addOnItemTouchListener(
                            new RecyclerItemClickListener(getActivity(), rec_sports, new RecyclerItemClickListener.OnItemClickListener() {
                                @Override
                                public void onItemClick(View view, int position) {
                                    Intent i = new Intent(getActivity(), MovieDetailActivity.class);
                                    Bundle mBundle = new Bundle();
                                    mBundle.putString("link", arrayList.get(position));
                                    i.putExtras(mBundle);
                                    startActivity(i);
                                }

                                @Override
                                public void onLongItemClick(View view, int position) {
                                    // do whatever
                                }
                            })
                    );
                    try {
                        rec_sports.setVisibility(View.VISIBLE);
                        shimmer_view_sports.stopShimmer();
                        shimmer_view_sports.setVisibility(View.GONE);
                    } catch (Exception ignored) {
                    }
                } else if (id.compareTo("37") == 0) {
                    rec_history.setHasFixedSize(true);
                    rec_history.setNestedScrollingEnabled(false);

                    rec_history.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));


                    HomepageAdapter adapter = new HomepageAdapter(getActivity(), arrayList, imageList);

                    rec_history.setAdapter(adapter);
                    // bannerIntialize();
                    Objects.requireNonNull(rec_history.getLayoutManager()).scrollToPosition(0);

                    rec_history.addOnItemTouchListener(
                            new RecyclerItemClickListener(getActivity(), rec_history, new RecyclerItemClickListener.OnItemClickListener() {
                                @Override
                                public void onItemClick(View view, int position) {
                                    Intent i = new Intent(getActivity(), MovieDetailActivity.class);
                                    Bundle mBundle = new Bundle();
                                    mBundle.putString("link", arrayList.get(position));
                                    i.putExtras(mBundle);
                                    startActivity(i);
                                }

                                @Override
                                public void onLongItemClick(View view, int position) {
                                    // do whatever
                                }
                            })
                    );
                    try {
                        rec_history.setVisibility(View.VISIBLE);
                        shimmer_view_history.stopShimmer();
                        shimmer_view_history.setVisibility(View.GONE);
                    } catch (Exception ignored) {
                    }
                } else if (id.compareTo("42") == 0) {
                    rec_documentry.setHasFixedSize(true);
                    rec_documentry.setNestedScrollingEnabled(false);

                    rec_documentry.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));


                    HomepageAdapter adapter = new HomepageAdapter(getActivity(), arrayList, imageList);

                    rec_documentry.setAdapter(adapter);
                    // bannerIntialize();
                    Objects.requireNonNull(rec_documentry.getLayoutManager()).scrollToPosition(0);

                    rec_documentry.addOnItemTouchListener(
                            new RecyclerItemClickListener(getActivity(), rec_documentry, new RecyclerItemClickListener.OnItemClickListener() {
                                @Override
                                public void onItemClick(View view, int position) {
                                    Intent i = new Intent(getActivity(), MovieDetailActivity.class);
                                    Bundle mBundle = new Bundle();
                                    mBundle.putString("link", arrayList.get(position));
                                    i.putExtras(mBundle);
                                    startActivity(i);
                                }

                                @Override
                                public void onLongItemClick(View view, int position) {
                                    // do whatever
                                }
                            })
                    );
                    try {
                        rec_documentry.setVisibility(View.VISIBLE);
                        shimmer_view_documentry.stopShimmer();
                        shimmer_view_documentry.setVisibility(View.GONE);
                    } catch (Exception ignored) {
                    }
                } else if (id.compareTo("41") == 0) {
                    rec_war.setHasFixedSize(true);
                    rec_war.setNestedScrollingEnabled(false);

                    rec_war.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));


                    HomepageAdapter adapter = new HomepageAdapter(getActivity(), arrayList, imageList);

                    rec_war.setAdapter(adapter);
                    // bannerIntialize();
                    Objects.requireNonNull(rec_war.getLayoutManager()).scrollToPosition(0);

                    rec_war.addOnItemTouchListener(
                            new RecyclerItemClickListener(getActivity(), rec_war, new RecyclerItemClickListener.OnItemClickListener() {
                                @Override
                                public void onItemClick(View view, int position) {
                                    Intent i = new Intent(getActivity(), MovieDetailActivity.class);
                                    Bundle mBundle = new Bundle();
                                    mBundle.putString("link", arrayList.get(position));
                                    i.putExtras(mBundle);
                                    startActivity(i);
                                }

                                @Override
                                public void onLongItemClick(View view, int position) {
                                    // do whatever
                                }
                            })
                    );
                    try {
                        rec_war.setVisibility(View.VISIBLE);
                        shimmer_view_war.stopShimmer();
                        shimmer_view_war.setVisibility(View.GONE);
                    } catch (Exception ignored) {
                    }
                } else if (id.compareTo("59") == 0) {
                    rec_webseries.setHasFixedSize(true);
                    rec_webseries.setNestedScrollingEnabled(false);

                    rec_webseries.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));


                    HomepageAdapter adapter = new HomepageAdapter(getActivity(), arrayList, imageList);

                    rec_webseries.setAdapter(adapter);
                    // bannerIntialize();
                    Objects.requireNonNull(rec_webseries.getLayoutManager()).scrollToPosition(0);

                    rec_webseries.addOnItemTouchListener(
                            new RecyclerItemClickListener(getActivity(), rec_webseries, new RecyclerItemClickListener.OnItemClickListener() {
                                @Override
                                public void onItemClick(View view, int position) {

                                    Intent i = new Intent(getActivity(), WebSeriesDetailActivity.class);
                                    Bundle mBundle = new Bundle();
                                    mBundle.putString("link", arrayList.get(position));
                                    i.putExtras(mBundle);
                                    startActivity(i);
                                }

                                @Override
                                public void onLongItemClick(View view, int position) {
                                    // do whatever
                                }
                            })
                    );
                    try {
                        rec_webseries.setVisibility(View.VISIBLE);
                        shimmer_view_webseries.stopShimmer();
                        shimmer_view_webseries.setVisibility(View.GONE);
                    } catch (Exception ignored) {
                    }
                }


                if (arrayList.size() == 0) {
                    Toast.makeText(getActivity(), "Sorry ! No Result Found.", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(@NotNull Call<List<PostsResponse>> call, @NotNull Throwable t) {

                t.printStackTrace();

                //   setImage1();
            }
        });
    }

    @Override
    public void OfferDetailsListener(int position) {
        if (BannerTypeList.get(position).equalsIgnoreCase("none")) {
            return;
        }
        if (BannerTypeList.get(position).equalsIgnoreCase("movie")) {
            Intent i = new Intent(getActivity(), MovieDetailActivity.class);
            Bundle mBundle = new Bundle();
            mBundle.putString("link", arrayList.get(position));
            i.putExtras(mBundle);
            startActivity(i);
        }

        if (BannerTypeList.get(position).equalsIgnoreCase("webseries")) {
            Intent i = new Intent(getActivity(), WebSeriesDetailActivity.class);
            Bundle mBundle = new Bundle();
            mBundle.putString("link", arrayList.get(position));
            i.putExtras(mBundle);
            startActivity(i);
        }

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }


    public void checkAppUpdate() {

        final int versionCode = BuildConfig.VERSION_CODE;

        final HashMap<String, Object> defaultMap = new HashMap<>();
        defaultMap.put(FB_RC_KEY_TITLE, "Update Available");
        defaultMap.put(FB_RC_KEY_DESCRIPTION, "A new version of the application is available please click below to update the latest version.");
        defaultMap.put(FB_RC_KEY_FORCE_UPDATE_VERSION, "" + versionCode);
        defaultMap.put(FB_RC_KEY_LATEST_VERSION, "" + versionCode);

        mFirebaseRemoteConfig = FirebaseRemoteConfig.getInstance();

        mFirebaseRemoteConfig.setConfigSettingsAsync(new FirebaseRemoteConfigSettings.Builder()
                .setMinimumFetchIntervalInSeconds(3600).
                        build());

        mFirebaseRemoteConfig.setDefaultsAsync(defaultMap);

        Task<Void> fetchTask = mFirebaseRemoteConfig.fetch(0/*BuildConfig.DEBUG ?  : TimeUnit.HOURS.toSeconds(4)*/);

        fetchTask.addOnCompleteListener(task -> {
            if (task.isSuccessful()) {

                mFirebaseRemoteConfig.activate();

                String title = getValue(FB_RC_KEY_TITLE, defaultMap);
                String description = getValue(FB_RC_KEY_DESCRIPTION, defaultMap);
                int forceUpdateVersion = Integer.parseInt(getValue(FB_RC_KEY_FORCE_UPDATE_VERSION, defaultMap));
                int latestAppVersion = Integer.parseInt(getValue(FB_RC_KEY_LATEST_VERSION, defaultMap));

                boolean isCancelable = true;

                if (latestAppVersion > versionCode) {
                    if (forceUpdateVersion > versionCode)
                        isCancelable = false;

                    appUpdateDialog = new AppUpdateDialog(getContext(), title, description, isCancelable);
                    appUpdateDialog.setCancelable(false);
                    appUpdateDialog.show();

                    Window window = appUpdateDialog.getWindow();
                    assert window != null;
                    window.setLayout(ConstraintLayout.LayoutParams.MATCH_PARENT, ConstraintLayout.LayoutParams.WRAP_CONTENT);

                }
            } else {
                Toast.makeText(getActivity(), "Fetch Failed",
                        Toast.LENGTH_SHORT).show();
            }
        });
    }

    public String getValue(String parameterKey, HashMap<String, Object> defaultMap) {
        String value = mFirebaseRemoteConfig.getString(parameterKey);
        if (TextUtils.isEmpty(value))
            value = (String) defaultMap.get(parameterKey);
        return value;
    }


}
