package com.exclusive.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.exclusive.MovieDetailActivity;
import com.exclusive.R;
import com.exclusive.WebSeriesDetailActivity;
import com.exclusive.adapter.OtherPageAdapter;
import com.exclusive.apis.Api;
import com.exclusive.models.PostsResponse;
import com.exclusive.utils.RecyclerItemClickListener;
import com.exclusive.utils.Utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class SearchFragment extends Fragment {
    private static final String TAG = "TAG";
    private RecyclerView recyclerView;
    final OkHttpClient okHttpClient = new OkHttpClient.Builder()
            .readTimeout(5, TimeUnit.MINUTES)
            .connectTimeout(5, TimeUnit.MINUTES)
            .writeTimeout(5, TimeUnit.MINUTES)
            .build();
    ArrayList<String> arrayList = new ArrayList<>();
    ArrayList<String> imageList = new ArrayList<>();
    ArrayList<List<Integer>> categories = new ArrayList<>();
    String value;
    EditText edtSearch;
    RelativeLayout relDummy;
    Toolbar toolbar;
    ImageView img_search;
    private View view;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_search, container, false);
        edtSearch = view.findViewById(R.id.edt_search);
        relDummy = view.findViewById(R.id.relDummy);
        toolbar = view.findViewById(R.id.toolbar);
        img_search = view.findViewById(R.id.img_search);
        toolbar.setVisibility(View.GONE);


        edtSearch.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                search();     // you can do anything
                return true;
            }
            return false;
        });


        img_search.setOnClickListener(view -> search());

        return view;
    }

    private void search() {
        InputMethodManager inputManager = (InputMethodManager) Objects.requireNonNull(getActivity()).getSystemService(Context.INPUT_METHOD_SERVICE);
        View viewf = getActivity().getCurrentFocus();
        if (view != null) {
            if (inputManager != null) {
                if (viewf != null) {
                    inputManager.hideSoftInputFromWindow(viewf.getWindowToken(), 0);
                }
            }
        }
        if (!edtSearch.getText().toString().trim().isEmpty()) {
            relDummy.setVisibility(View.GONE);
            loading(true);
            setImage(edtSearch.getText().toString());
        }


    }


    public void loading(boolean loading) {
        if (loading) {
            view.findViewById(R.id.loading).setVisibility(View.VISIBLE);
            view.findViewById(R.id.recyclerViewResult).setVisibility(View.GONE);
        } else {
            view.findViewById(R.id.loading).setVisibility(View.GONE);
            view.findViewById(R.id.recyclerViewResult).setVisibility(View.VISIBLE);
        }
    }

    private void setImage(String string) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Api.BASE_URL)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create()) //Here we are using the GsonConverterFactory to directly convert json data to object
                .build();

        Api api = retrofit.create(Api.class);
        Call<List<PostsResponse>> call = null;

        call = api.postsBySearch2(string, Utils.getOptions());
        call.enqueue(new Callback<List<PostsResponse>>() {
            @Override
            public void onResponse(Call<List<PostsResponse>> call, Response<List<PostsResponse>> response) {
                arrayList.clear();
                imageList.clear();
                categories.clear();

                if (response.body() != null && response.body().size() != 0) {
                    for (int i = 0; i < response.body().size(); i++) {
                        arrayList.add("" + response.body().get(i).getId());
                        imageList.add("" + (response.body().get(i).getJetpackFeaturedMediaUrl()));
                        categories.add((response.body().get(i).getCategories()));
                    }
                }
                setRecyclerView();

            }

            @Override
            public void onFailure(Call<List<PostsResponse>> call, Throwable t) {
                Toast.makeText(getActivity(), t.getMessage(), Toast.LENGTH_SHORT).show();
                Toast.makeText(getActivity(), "Failed", Toast.LENGTH_LONG).show();
            }
        });
    }

    private void setRecyclerView() {

        recyclerView = view.findViewById(R.id.recyclerViewResult);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 3));//Linear Items


        OtherPageAdapter adapter = new OtherPageAdapter(getActivity(), arrayList, imageList);

        recyclerView.setAdapter(adapter);// set adapter on recyclerview
        loading(false);
        recyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(getActivity(), recyclerView, new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        Intent i;
                        if (categories.get(position).contains(59)) {
                            i = new Intent(getActivity(), WebSeriesDetailActivity.class);
                        } else {
                            i = new Intent(getActivity(), MovieDetailActivity.class);
                        }
                        Bundle mBundle = new Bundle();
                        mBundle.putString("link", arrayList.get(position));
                        i.putExtras(mBundle);
                        startActivity(i);
                    }

                    @Override
                    public void onLongItemClick(View view, int position) {
                        // do whatever
                    }
                })
        );


        if (arrayList.size() == 0) {
            Toast.makeText(getActivity(), "Sorry ! No Result Found.", Toast.LENGTH_LONG).show();
        }

    }


}
