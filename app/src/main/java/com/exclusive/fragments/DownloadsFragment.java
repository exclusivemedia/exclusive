package com.exclusive.fragments;

import android.Manifest;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.exclusive.R;
import com.exclusive.adapter.DownloadsAdapter;
import com.exclusive.downloads.DirectoryHelper;
import com.exclusive.models.DownloadedVideoModel;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class DownloadsFragment extends Fragment implements DownloadsAdapter.DetailsListener {

    private final ArrayList<DownloadedVideoModel> al_video = new ArrayList<>();
    private RecyclerView recyclerView;
    private AppCompatTextView noDownloads;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_downloads, container, false);

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //al_video.clear();
        //noDownloads = view.findViewById(R.id.noDownloads);
        //noDownloads.setVisibility(View.VISIBLE);
        // recyclerView = view.findViewById(R.id.recycler_view1);
        // RecyclerView.LayoutManager recyclerViewLayoutManager = new LinearLayoutManager(getActivity());
        // recyclerView.setLayoutManager(recyclerViewLayoutManager);
        //requestStoragePermission();
    }


    private void requestStoragePermission() {
        Dexter.withContext(getActivity())
                .withPermissions(
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        // check if all permissions are granted
                        if (report.areAllPermissionsGranted()) {
                            //      Toast.makeText(getApplicationContext(), "All permissions are granted!", Toast.LENGTH_SHORT).show();
                            fn_video();
                        }

                        // check for permanent denial of any permission
                        if (report.isAnyPermissionPermanentlyDenied()) {
                            // show alert dialog navigating to Settings
                            showSettingsDialog();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).
                withErrorListener(error -> Toast.makeText(getActivity(), "Error occurred! ", Toast.LENGTH_SHORT).show())
                .onSameThread()
                .check();
    }

    private void showSettingsDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(Objects.requireNonNull(getActivity()));
        builder.setTitle("Need Permissions");
        builder.setMessage("This app needs permission to use this feature. You can grant them in app settings.");
        builder.setPositiveButton("GOTO SETTINGS", (dialog, which) -> {
            dialog.cancel();
            openSettings();
        });
        builder.setNegativeButton("Cancel", (dialog, which) -> dialog.cancel());
        builder.show();
    }

    // navigating user to app settings
    private void openSettings() {
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", Objects.requireNonNull(getActivity()).getPackageName(), null);
        intent.setData(uri);
        startActivityForResult(intent, 101);
    }


    public void fn_video() {

        String path = Environment.getExternalStorageDirectory().toString() + "/" + DirectoryHelper.ROOT_DIRECTORY_NAME;
        Log.e("Files", "Path: " + path);
        File directory = new File(path);
        if (!directory.exists()) {
            noDownloads.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
            return;
        }
        File[] files = directory.listFiles();
        if ((files != null ? files.length : 0) < 1) {
            noDownloads.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
            return;
        }
        Log.d("Files", "Size: " + files.length);
        al_video.clear();
        for (File file : files) {
            Log.d("Files", "FileName:" + file.getName());


            DownloadedVideoModel obj_model = new DownloadedVideoModel();
            obj_model.setBoolean_selected(false);
            obj_model.setStr_path(file.getAbsolutePath());
            obj_model.setStr_thumb(file.getPath());
            obj_model.setStr_name(file.getName());

            al_video.add(obj_model);


        }
        noDownloads.setVisibility(View.GONE);
        recyclerView.setVisibility(View.VISIBLE);
        DownloadsAdapter obj_adapter = new DownloadsAdapter(getActivity(),
                al_video, getActivity(), this);
        recyclerView.setAdapter(obj_adapter);

    }

    @Override
    public void OfferDetailsListener(int position) {

        Log.d("POs", String.valueOf(position));
    /*    if (position == 0) {
            noDownloads.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
        }*/

    }


}
