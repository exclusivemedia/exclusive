package com.exclusive.models;

public class EpisodeModel {
    String previewImage = "";
    String title = "";
    String videourl_hd;
    String videourl_sd;
    String videourl_low;
    String episode_subtitle = "";

    public String getEpisode_subtitle() {
        return episode_subtitle;
    }

    public void setEpisode_subtitle(String episode_subtitle) {
        this.episode_subtitle = episode_subtitle;
    }

    public String getPreviewImage() {
        return previewImage;
    }

    public void setPreviewImage(String previewImage) {
        this.previewImage = previewImage;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getVideourl_hd() {
        return videourl_hd;
    }

    public void setVideourl_hd(String videourl_hd) {
        this.videourl_hd = videourl_hd;
    }

    public String getVideourl_sd() {
        return videourl_sd;
    }

    public void setVideourl_sd(String videourl_sd) {
        this.videourl_sd = videourl_sd;
    }

    public String getVideourl_low() {
        return videourl_low;
    }

    public void setVideourl_low(String videourl_low) {
        this.videourl_low = videourl_low;
    }
}
